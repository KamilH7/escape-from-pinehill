using UnityEngine;
using UnityEngine.UI;

namespace UI.InspectorPanel
{
    public class InspectorPanel : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Image inspectorPanelImage;
        [SerializeField]
        private Text inspectorPanelText;

        #endregion

        #region Private Fields

        private bool isInspected;

        #endregion

        #region Public Properties

        public bool IsInspected { get => isInspected; }

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            InitializeValues();
        }

        #endregion

        #region Public Methods

        public void ShowInspectorPanel(Sprite sprite, string text)
        {
            isInspected = true;

            inspectorPanelImage.sprite = sprite;
            inspectorPanelText.text = text;

            inspectorPanelImage.enabled = true;
            inspectorPanelText.enabled = true;
        }

        public void HideInspectorPanel()
        {
            isInspected = false;
            inspectorPanelImage.enabled = false;
            inspectorPanelText.enabled = false;
        }

        #endregion

        #region Private Methods

        private void InitializeValues()
        {
            isInspected = false;
        }

        #endregion
    }
}