﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Data
{
    [CreateAssetMenu(menuName = "UI/Create UIEvents", fileName = "UIEvents", order = 0)]
    public class UIEvents : ScriptableObject
    {
        #region Private Fields

        private Dictionary<UIEventType, Action> callbackByEventType = new Dictionary<UIEventType, Action>();

        #endregion

        #region Public Methods

        public void AddListener(UIEventType eventType, Action callback)
        {
            if (!callbackByEventType.ContainsKey(eventType))
            {
                callbackByEventType[eventType] = () => { };
            }

            callbackByEventType[eventType] += callback;
        }

        public void RemoveListener(UIEventType eventType, Action callback)
        {
            if (!callbackByEventType.ContainsKey(eventType))
            {
                callbackByEventType[eventType] = () => { };
            }

            callbackByEventType[eventType] -= callback;
        }

        public void Invoke(UIEventType eventType)
        {
            if (!callbackByEventType.ContainsKey(eventType))
            {
                return;
            }

            callbackByEventType[eventType]?.Invoke();
        }

        #endregion
    }
}