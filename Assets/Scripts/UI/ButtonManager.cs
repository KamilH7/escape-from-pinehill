﻿using UI.Data;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class ButtonManager : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private UIEvents uiEvents;
        [SerializeField]
        private UIEventType eventType;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            AssignCallback();
        }

        #endregion

        #region Private Methods

        private void AssignCallback()
        {
            GetComponent<Button>().onClick.AddListener(Confirm);
        }

        private void Confirm()
        {
            uiEvents.Invoke(eventType);
        }

        #endregion
    }
}