using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Inventory.Data
{
    [CreateAssetMenu(menuName = "Inventory/Create Inventory Data", fileName = "Inventory")]
    public class PlayerInventory : ScriptableObject
    {
        #region Events

        public event Action OnInventoryChanged;
        public event Action<Item> OnAdded;
        public event Action<Item> OnRemoved;

        #endregion

        #region Serialized Fields

        [SerializeField]
        private List<Item> items = new List<Item>();
        [SerializeField]
        private int bulletsAmount;
        [SerializeField]
        private int idCardsAmount;

        #endregion

        #region Public Properties

        public IReadOnlyList<Item> Items => items;
        public int ItemsAmount => items.Count;

        public int BulletsAmount => bulletsAmount;
        public int IDCardsAmount => idCardsAmount;
        public bool HasBullets => bulletsAmount > 0;
        public bool HasIdCards => idCardsAmount > 0;

        #endregion

        #region Public Methods

        public void AddItem(Item item)
        {
            if (HasItem(item))
            {
                return;
            }

            items.Add(item);
            OnAdded?.Invoke(item);
            OnInventoryChanged?.Invoke();
        }

        [Button]
        public void AddBullets(int amount)
        {
            bulletsAmount += amount;
        }

        [Button]
        public void AddIdCards(int amount)
        {
            idCardsAmount += amount;
        }

        public void RemoveItem(Item item)
        {
            items.Remove(item);
            OnRemoved?.Invoke(item);
            OnInventoryChanged?.Invoke();
        }

        [Button]
        public void UseBullet()
        {
            bulletsAmount = Math.Max(0, bulletsAmount - 1);
        }

        [Button]
        public void UseIdCard()
        {
            idCardsAmount = Math.Max(0, idCardsAmount - 1);
        }

        [Button]
        public void RemoveAllItems()
        {
            bulletsAmount = 0;
            idCardsAmount = 0;
            RemoveInventoryItems();
        }

        public void RemoveInventoryItems()
        {
            items.Clear();
            OnInventoryChanged?.Invoke();
        }
        public bool HasItem(Item item)
        {
            return items.Contains(item);
        }

        #endregion
    }
}