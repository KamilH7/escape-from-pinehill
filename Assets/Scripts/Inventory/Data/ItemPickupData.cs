using UnityEngine;

namespace Inventory.Data
{
    [CreateAssetMenu(menuName = "Inventory/Create Last Item Data", fileName = "LastPickupItemData")]
    public class ItemPickupData : ScriptableObject
    {
        #region Serialized Fields

        [SerializeField]
        private string lastPickedupItem;

        #endregion

        #region Public Properties

        public string LastPickedupItem { get => lastPickedupItem; set => lastPickedupItem = value; }

        #endregion
    }
}