﻿using UnityEngine;

namespace Inventory.Data
{
    [CreateAssetMenu(menuName = "Inventory/Create Item", fileName = "Item")]
    public class Item : ScriptableObject
    {
        #region Serialized Fields

        [SerializeField]
        private string itemName;
        [SerializeField]
        private Sprite sprite;

        #endregion

        #region Public Properties

        public Sprite Sprite => sprite;

        public string ItemName => itemName;

        #endregion
    }
}