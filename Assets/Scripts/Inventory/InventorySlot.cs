using Inventory.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Inventory
{
    public class InventorySlot : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Image icon;

        #endregion

        #region Private Fields

        private Item item;

        #endregion

        #region Public Methods

        public void SetItem(Item newItem)
        {
            item = newItem;

            icon.sprite = item.Sprite;
            icon.enabled = true;
        }

        public void ClearSlot()
        {
            item = null;

            icon.sprite = null;
            icon.enabled = false;
        }

        #endregion
    }
}