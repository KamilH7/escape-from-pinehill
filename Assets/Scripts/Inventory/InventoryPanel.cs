using GameLoop.Data;
using Inventory.Data;
using TMPro;
using UnityEngine;

namespace Inventory
{
    public class InventoryPanel : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private PlayerInventory playerInventory;
        [SerializeField]
        private Transform inventoryItemsParent;
        [SerializeField]
        private TextMeshProUGUI bulletAmountText;
        [SerializeField]
        private TextMeshProUGUI idCardAmountText;

        #endregion

        #region Private Fields

        private InventorySlot[] inventorySlots;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
        }

        private void Start()
        {
            AssignCallback();
            UpdatePlayerInventoryDisplay();
        }

        private void Update()
        {
            UpdateUITextFields();
        }

        private void OnDestroy()
        {
            UnassignCallback();
        }

        #endregion

        #region Private Methods

        private void UpdatePlayerInventoryDisplay()
        {
            for (int i = 0; i < inventorySlots.Length; i++)
            {
                if (i < playerInventory.ItemsAmount)
                {
                    inventorySlots[i].SetItem(playerInventory.Items[i]);
                }
                else
                {
                    inventorySlots[i].ClearSlot();
                }
            }
        }

        private void AssignCallback()
        {
            playerInventory.OnInventoryChanged += UpdatePlayerInventoryDisplay;
            GameEvents.OnNextLevelPassed += () => playerInventory.RemoveInventoryItems();
        }

        private void UnassignCallback()
        {
            playerInventory.OnInventoryChanged -= UpdatePlayerInventoryDisplay;
        }

        private void GetReferences()
        {
            inventorySlots = inventoryItemsParent.GetComponentsInChildren<InventorySlot>();
        }

        private void UpdateUITextFields()
        {
            bulletAmountText.text = $"x{playerInventory.BulletsAmount:000}";
            idCardAmountText.text = $"x{playerInventory.IDCardsAmount:000}";
        }

        #endregion
    }
}