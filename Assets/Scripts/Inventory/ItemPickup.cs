using Animation;
using Inventory.Data;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace Inventory
{
    public class ItemPickup : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private PlayerInventory inventory;
        [SerializeField]
        private Item item;

        #endregion

        #region Private Fields

        private AnimationTrigger animationTrigger;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
            Enable();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer())
            {
                PickedUp();
                animationTrigger.TriggerAnimations();
            }
        }

        #endregion

        #region Public Methods

        public void AnimationDisableEvent()
        {
            Disable();
        }

        #endregion

        #region Private Methods

        [Button]
        private void Enable()
        {
            gameObject.SetActive(true);
        }

        [Button]
        private void TriggerAnimations()
        {
            animationTrigger.TriggerAnimations();
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void PickedUp()
        {
            inventory.AddItem(item);
        }

        private void GetReferences()
        {
            animationTrigger = GetComponent<AnimationTrigger>();
        }

        #endregion
    }
}