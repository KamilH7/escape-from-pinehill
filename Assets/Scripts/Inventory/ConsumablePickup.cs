using Animation;
using Inventory.Data;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace Inventory
{
    public class ConsumablePickup : MonoBehaviour
    {
        #region Enums

        private enum ConsumableType
        {
            ID,
            Bullet
        }

        #endregion

        #region Serialized Fields

        [SerializeField]
        private PlayerInventory inventory;
        [SerializeField]
        private ConsumableType type;
        [SerializeField]
        private int amount;

        #endregion

        #region Private Fields

        private AnimationTrigger animationTrigger;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
            Enable();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer())
            {
                PickedUp();
                animationTrigger.TriggerAnimations();
                GetComponent<Collider>().enabled = false;
            }
        }

        #endregion

        #region Public Methods

        public void AnimationDisableEvent()
        {
            Disable();
        }

        #endregion

        #region Private Methods

        [Button]
        private void Enable()
        {
            gameObject.SetActive(true);
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void PickedUp()
        {
            switch (type)
            {
                case ConsumableType.ID :
                    Debug.Log($"Pickedup {amount} IDs");
                    inventory.AddIdCards(amount);

                    break;
                case ConsumableType.Bullet :
                    Debug.Log($"Pickedup {amount} bullets");
                    inventory.AddBullets(amount);

                    break;
                default :
                    Debug.LogError("Unsupported consumable type in: " + transform.name);

                    break;
            }
        }

        private void GetReferences()
        {
            animationTrigger = GetComponent<AnimationTrigger>();
        }

        #endregion
    }
}