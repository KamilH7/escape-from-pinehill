using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace Puzzle
{
    public class PressurePlate : MonoBehaviour
    {
        #region Events
        
        public event Action<Vector2Int> OnPlatePressed;

        #endregion

        #region Private Fields

        private PlayerCollisionDetector collisionDetector;
        private Renderer renderer;

        private static readonly string ColorParameter = "_Color";

        private bool isPuzzleOver = false;
        #endregion

        #region Public Properties

        [ShowInInspector]
        public Vector2Int Position { get; set; }

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
            AssignCallback();
        }

        private void OnDestroy()
        {
            UnassignCallback();
        }

        #endregion

        #region Public Methods

        public void PuzzleOver()
        {
            ChangePlateColor(Color.white);
            isPuzzleOver = true;
        }

        #endregion

        #region Private Methods

        private void ChangePlateColor(Color color)
        {
            renderer.material.SetColor(ColorParameter, color);
        }

        private void PlatePressed()
        {
            if (!isPuzzleOver)
            {
                ChangePlateColor(Color.red);
                OnPlatePressed?.Invoke(Position);
            }
        }

        private void GetReferences()
        {
            collisionDetector = GetComponentInChildren<PlayerCollisionDetector>();
            renderer = GetComponentInChildren<Renderer>();
        }

        private void AssignCallback()
        {
            collisionDetector.OnPlayerEnter += PlatePressed;
        }

        private void UnassignCallback()
        {
            collisionDetector.OnPlayerEnter -= PlatePressed;
        }

        #endregion
    }
}