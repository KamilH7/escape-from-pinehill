using System.Collections.Generic;
using GameLoop.Data;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Puzzle
{
    public class TilePuzzle : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private PressurePlate tilePrefab;
        [SerializeField]
        private Vector2Int tileCount;
        [SerializeField]
        private Vector2 tileOffset;

        #endregion

        #region Private Fields

        private List<PressurePlate> tiles = new List<PressurePlate>();
        private List<Vector2Int> pressedTilePositionsSequence = new List<Vector2Int>();
        private TilePuzzlePathGenerator pathGenerator;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
        }

        private void Start()
        {
            for (int y = 0; y < tileCount.y; ++y)
            {
                for (int x = 0; x < tileCount.x; ++x)
                {
                    InstantiateTile(x, y);
                }
            }

            foreach (PressurePlate tile in tiles)
            {
                tile.OnPlatePressed += TilePressed;
            }

            pathGenerator.GeneratePath(tileCount);
        }

        private void OnDrawGizmos()
        {
            Vector3 scale = tilePrefab.transform.localScale;
            Gizmos.color = new Color(1, 0, 0, 0.5f);

            for (int y = 0; y < tileCount.y; ++y)
            {
                for (int x = 0; x < tileCount.x; ++x)
                {
                    Vector3 position = new Vector3(x * tileOffset.x, 0, y * tileOffset.y);
                    position = transform.TransformDirection(position);

                    Gizmos.DrawCube(transform.position + position, scale * 0.99f);
                }
            }
        }

        #endregion

        #region Private Methods

        private void InstantiateTile(int x, int y)
        {
            PressurePlate tile = Instantiate(tilePrefab, new Vector3(), Quaternion.identity, transform);
            tile.transform.localPosition = new Vector3(x * tileOffset.x, 0, y * tileOffset.y);

            tile.Position = new Vector2Int(x + 1, y + 1);

            tiles.Add(tile);
        }

        private void TilePressed(Vector2Int position)
        {
            if (WasTileAlreadyPressed(position))
            {
                return;
            }

            if (!IsTileInSequence(position))
            {
                GameEvents.PlayerDied();
                Debug.Log("Player stepped on wrong tile");

                return;
            }

            pressedTilePositionsSequence.Add(position);
        }

        private bool WasTileAlreadyPressed(Vector2Int position)
        {
            return pressedTilePositionsSequence.Contains(position);
        }

        private bool IsTileInSequence(Vector2Int position)
        {
            if (IsSequenceDone())
            {
                return false;
            }

            return pathGenerator.Path[pressedTilePositionsSequence.Count] == position;
        }

        private bool IsSequenceDone()
        {
            return pathGenerator.PathLength == pressedTilePositionsSequence.Count;
        }

        private void GetReferences()
        {
            pathGenerator = GetComponent<TilePuzzlePathGenerator>();
        }

        public void EndPuzzle()
        {

            foreach (PressurePlate tile in tiles)
            {
                tile.PuzzleOver();
            }
        }

        #endregion
    }
}