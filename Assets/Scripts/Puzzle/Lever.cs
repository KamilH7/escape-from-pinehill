using Animation;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace Puzzle
{
    public class Lever : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private InputActionReference input;

        [SerializeField]
        private PlayerCollisionDetector collisionDetector;

        [SerializeField]
        private AnimationTrigger animationTrigger;

        [SerializeField]
        private float timeoutDuration = 3.0f;

        #endregion

        #region Private Fields

        private bool playerInside;
        private float timeout;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            AssignInvokes();
        }

        private void Update()
        {
            if (timeout <= 0)
            {
                if (playerInside && IsInteracting())
                {
                    timeout = timeoutDuration;
                    Activate();
                }
            }
            else
            {
                timeout -= Time.deltaTime;
            }
        }

        #endregion

        #region Public Methods

        [Button]
        public void Activate()
        {
            animationTrigger.TriggerAnimations();
        }

        #endregion

        #region Private Methods

        private bool IsInteracting()
        {
            return input.action.triggered;
        }

        private void AssignInvokes()
        {
            collisionDetector.OnPlayerEnter += PlayerEntered;
            collisionDetector.OnPlayerExit += PlayerLeft;
        }

        private void PlayerEntered()
        {
            playerInside = true;
        }

        private void PlayerLeft()
        {
            playerInside = false;
        }

        #endregion
    }
}