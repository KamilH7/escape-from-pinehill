using UI.InspectorPanel;
using UnityEngine;
using Utils.DataVariables;

namespace Puzzle
{
    public class InspectableObject : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Sprite inspectedItemImage;
        [SerializeField, TextArea]
        private string inspectedItemText;
        [SerializeField]
        private StringVariable textToDisplayVariable;

        #endregion

        #region Private Fields

        private GameObject inspectorPanel;
        private InspectorPanel inspectorController;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            GetReferences();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                if (!inspectorController.IsInspected)
                {
                    ShowInspectorPanel();
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                HideInspectorPanel();
            }
        }

        #endregion

        #region Private Methods

        private void GetReferences()
        {
            inspectorPanel = GameObject.Find("InspectorPanel");
            inspectorController = inspectorPanel.GetComponent<InspectorPanel>();
        }

        private void ShowInspectorPanel()
        {
            inspectorController.ShowInspectorPanel(inspectedItemImage, textToDisplayVariable.Value);
        }

        private void HideInspectorPanel()
        {
            inspectorController.HideInspectorPanel();
        }

        #endregion
    }
}