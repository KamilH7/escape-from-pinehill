using System;
using System.Collections.Generic;
using Puzzle.Data;
using UnityEngine;
using Utils.DataVariables;
using Random = UnityEngine.Random;

namespace Puzzle
{
    public class TilePuzzlePathGenerator : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        public StudentNameData names;
        [SerializeField]
        private StringVariable studentRecordsVariable;

        #endregion

        #region Private Fields

        private List<StudentRecord> studentRecords = new List<StudentRecord>();
        private List<Vector2Int> path = new List<Vector2Int>();
        private string textTitle = "Lp.|Imie i nazwisko|Ocena\n \n";

        #endregion

        #region Public Properties

        public IReadOnlyList<StudentRecord> StudentRecords => studentRecords;
        public IReadOnlyList<Vector2Int> Path => path;
        public int PathLength => path.Count;

        #endregion

        #region Public Methods

        public void GeneratePath(Vector2Int tileCount)
        {
            GenerateRecords(tileCount);
            GeneratePathSequence();
        }

        #endregion

        #region Private Methods

        private void GenerateRecords(Vector2Int tileCount)
        {
            for (int i = 0; i < tileCount.x; ++i)
            {
                studentRecords.Add(new StudentRecord(i + 1, Random.Range(1, tileCount.y + 1), names.GetRandomName()));
            }

            string output = textTitle;

            foreach (StudentRecord record in studentRecords)
            {
                output += $"{record.ID}. {record.Name} {record.Grade} \n";
            }

            studentRecordsVariable.Value = output;
        }

        private void GeneratePathSequence()
        {
            path.Add(new Vector2Int(1, studentRecords[0].Grade));

            for (int x = 1; x < studentRecords.Count; ++x)
            {
                int gradeDifference = studentRecords[x].Grade - studentRecords[x - 1].Grade;
                bool isGoingUp = gradeDifference > 0;

                for (int y = 0; Math.Abs(y) != Math.Abs(gradeDifference) + 1;)
                {
                    path.Add(new Vector2Int(x + 1, studentRecords[x - 1].Grade + y));

                    y += isGoingUp ? 1 : -1;
                }
            }
        }

        #endregion
    }
}