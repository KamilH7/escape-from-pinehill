using System.Collections.Generic;
using UnityEngine;

namespace Puzzle.Data
{
    [CreateAssetMenu(menuName = "Puzzle/Tile/StudentNameData")]
    public class StudentNameData : ScriptableObject
    {
        #region Serialized Fields

        [SerializeField]
        private List<string> firstNames;
        [SerializeField]
        private List<string> lastNames;

        #endregion

        #region Public Methods

        public string GetRandomName()
        {
            return firstNames[Random.Range(0, firstNames.Count)] + " " + lastNames[Random.Range(0, lastNames.Count)];
        }

        #endregion
    }
}