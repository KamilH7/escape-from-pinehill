namespace Puzzle.Data
{
    public struct StudentRecord
    {
        #region Public Properties

        public int ID { get; }
        public int Grade { get; }
        public string Name { get; }

        #endregion

        #region Constructors

        public StudentRecord(int id, int grade, string name)
        {
            ID = id;
            Grade = grade;
            Name = name;
        }

        #endregion
    }
}