using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using Puzzle;

public class EndTilePuzzle : MonoBehaviour
{
    [SerializeField]
    PlayerCollisionDetector detector;
    [SerializeField]
    TilePuzzle tilePuzzle;

    void Start()
    {
        detector.OnPlayerEnter += PlayerEntered;
    }

    void PlayerEntered()
    {
        tilePuzzle.EndPuzzle();
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        detector.OnPlayerEnter -= PlayerEntered;
    }
}
