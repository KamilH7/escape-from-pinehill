using System;
using UnityEngine;
using Utils;

namespace Puzzle
{
    public class PressurePlateColor : MonoBehaviour
    {
        #region Events

        public event Action<PressurePlateColor> OnPlateEnter;
        public event Action<PressurePlateColor> OnPlateExit;

        #endregion

        #region Private Fields

        private PlayerCollisionDetector collisionDetector;
        private Renderer renderer;

        private static readonly string ColorParameter = "_Color";

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
            AssignCallback();
        }

        private void OnDestroy()
        {
            UnassignCallback();
        }

        #endregion

        #region Public Methods

        public void ResetPlate()
        {
            ChangePlateColor(Color.white);
        }

        public void ChangePlateColor(Color color)
        {
            renderer.material.SetColor(ColorParameter, color);
        }

        #endregion

        #region Private Methods

        private void PlateEnter()
        {
            OnPlateEnter?.Invoke(this);
        }

        private void PlateExit()
        {
            OnPlateExit?.Invoke(this);
        }

        private void GetReferences()
        {
            collisionDetector = GetComponentInChildren<PlayerCollisionDetector>();
            renderer = GetComponentInChildren<Renderer>();
        }

        private void AssignCallback()
        {
            collisionDetector.OnPlayerEnter += PlateEnter;
            collisionDetector.OnPlayerExit += PlateExit;
        }

        private void UnassignCallback()
        {
            collisionDetector.OnPlayerEnter -= PlateEnter;
            collisionDetector.OnPlayerExit -= PlateExit;
        }

        #endregion
    }
}