using System.Collections.Generic;
using GameLoop.Data;
using UnityEngine;

namespace Puzzle
{
    public class ColorPuzzle : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private List<PressurePlateColor> plates;
        [SerializeField]
        private int maxSequence;
        [SerializeField]
        private bool isPuzzleComplete;
        [SerializeField]
        private float showNextElementCooldown = 1.0f;
        [SerializeField]
        private float showSequenceCooldown = 1.0f;

        #endregion

        #region Private Fields

        private List<PressurePlateColor> sequence;
        private int sequenceIndex;

        private bool isShowingSequence;
        private int showElementIndex;
        private float showNextElementTime;
        private float showSequenceTime;

        #endregion

        #region Public Properties

        public bool IsPuzzleComplete { get; }

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            foreach (PressurePlateColor plate in plates)
            {
                plate.OnPlateEnter += TileEnter;
                plate.OnPlateExit += TileExit;
            }

            InitializeSequence();
            ShowSequence();
        }

        private void Update()
        {
            if (isShowingSequence && IsTimeToShowNextElement() && IsTimeToShowSequence())
            {
                if (showElementIndex != 0)
                {
                    sequence[showElementIndex - 1].ResetPlate();
                }

                if (showElementIndex == sequence.Count)
                {
                    isShowingSequence = false;

                    return;
                }

                sequence[showElementIndex].ChangePlateColor(Color.red);

                showNextElementTime = Time.time + showNextElementCooldown;
                showElementIndex += 1;
            }
        }

        #endregion

        #region Private Methods

        private bool IsTimeToShowSequence()
        {
            return showSequenceTime < Time.time;
        }

        private bool IsTimeToShowNextElement()
        {
            return showNextElementTime < Time.time;
        }

        private void AddRandomElementToSequence()
        {
            if (sequence.Count == 0)
            {
                sequence.Add(plates[Random.Range(0, plates.Count)]);

                return;
            }

            PressurePlateColor randElement = sequence[sequence.Count - 1];

            while (randElement == sequence[sequence.Count - 1])
            {
                randElement = plates[Random.Range(0, plates.Count)];
            }

            sequence.Add(randElement);
        }

        private void InitializeSequence()
        {
            sequence = new List<PressurePlateColor>();
            AddRandomElementToSequence();
            AddRandomElementToSequence();
            sequenceIndex = 0;
        }

        private void GenerateNextSequence()
        {
            sequenceIndex = 0;
            AddRandomElementToSequence();
        }

        private void TileEnter(PressurePlateColor pressedPlate)
        {
            if (isPuzzleComplete || isShowingSequence)
            {
                return;
            }

            pressedPlate.ChangePlateColor(Color.green);

            if (sequence[sequenceIndex] != pressedPlate)
            {
                GameEvents.PlayerDied();
                Debug.Log("Stepped on wrong plate");

                return;
            }

            sequenceIndex += 1;

            if (sequenceIndex == sequence.Count)
            {
                SequenceComplete();
            }
        }

        private void TileExit(PressurePlateColor pressedPlate)
        {
            if (!isShowingSequence || !IsTimeToShowSequence())
            {
                pressedPlate.ResetPlate();
            }
        }

        private void SequenceComplete()
        {
            if (sequence.Count >= maxSequence)
            {
                isPuzzleComplete = true;

                return;
            }

            GenerateNextSequence();
            ShowSequence();
        }

        private void ShowSequence()
        {
            isShowingSequence = true;

            showElementIndex = 0;
            showNextElementTime = 0;

            showSequenceTime = Time.time + showSequenceCooldown;
        }

        #endregion
    }
}