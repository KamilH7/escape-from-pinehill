using System.Collections.Generic;
using Inventory.Data;
using UnityEngine;
using Utils;

public class RequirementCheck : MonoBehaviour
{
    #region Serialized Fields

    [SerializeField]
    private List<Item> requiredItems;
    [SerializeField]
    private PlayerInventory inventory;

    #endregion

    #region Unity Callbacks

    private void OnTriggerEnter(Collider other)
    {
        if (other.IsPlayer())
        {
            if (!HasAllRequiredItems())
            {
                RequirementsNotMetNotification();
            }
            else
            {
                RequirementsMetNotification();
            }
        }
    }

    #endregion

    #region Private Methods

    private bool HasAllRequiredItems()
    {
        bool hasAllRequiredItems = true;

        foreach (Item item in requiredItems)
        {
            if (!inventory.HasItem(item))
            {
                hasAllRequiredItems = false;
            }
        }

        return hasAllRequiredItems;
    }

    private void RequirementsNotMetNotification()
    {
        Debug.Log("Player doesn't have required items");
    }

    private void RequirementsMetNotification()
    {
        Debug.Log("Player has required items");
    }

    #endregion
}