using System;
using UnityEngine;

namespace DialogueSystem
{
    [Serializable]
    public class Dialogue
    {
        #region Serialized Fields

        [SerializeField, Range(0, 1)]
        public float pivotXPosition = 1;
        [SerializeField, Range(0, 1)]
        public float pivotYPosition;

        #endregion

        public string narrator = "";
        [TextArea]
        public string text = "";

        [NonSerialized]
        public Vector3 worldSpacePosition;
    }
}