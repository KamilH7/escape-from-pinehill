using UnityEngine;
using Utils.Triggers;

namespace DialogueSystem
{
    public class DialogueController : MonoBehaviour
    {
        #region Serialized Fields

        [Header("UI References"), SerializeField]
        private DialogueBox dialogueBox;

        [Header("Dialogue Data References"), SerializeField]
        private TriggerEvent dialogueChangedEvent;
        [SerializeField]
        private CurrentDialogueData dialogueData;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            AssignCallback();
        }

        private void OnDestroy()
        {
            UnassignCallback();
        }

        #endregion

        #region Public Methods

        public void DialogueUpdated()
        {
            dialogueBox.ChangeDialogue(dialogueData.dialogue);
        }

        #endregion

        #region Private Methods

        private void AssignCallback()
        {
            dialogueChangedEvent.AddListener(DialogueUpdated);
            dialogueData.OnClosed += CloseDialogue;
        }

        private void CloseDialogue()
        {
            dialogueBox.DisableContainer();
        }

        private void UnassignCallback()
        {
            dialogueChangedEvent.RemoveListener(DialogueUpdated);
            dialogueData.OnClosed -= CloseDialogue;
        }

        #endregion
    }
}