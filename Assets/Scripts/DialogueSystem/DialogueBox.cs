using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DialogueSystem
{
    public class DialogueBox : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Dialogue displayedDialogue = new Dialogue();

        [Header("References"), SerializeField]
        private GameObject containerObject;
        [SerializeField]
        private TextMeshProUGUI narratorTMP;
        [SerializeField]
        private TextMeshProUGUI dialogueTMP;

        [Header("To fix weird UI glitch"), SerializeField]
        private RectTransform[] LayoutGroupsToRefresh;

        #endregion

        #region Private Fields

        private RectTransform boxRectTransform;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            GetReferences();
            DisableContainer();
        }

        private void Update()
        {
            UpdateDialogueBox();
        }

        #endregion

        #region Public Methods

        public void ChangeDialogue(Dialogue dialogue)
        {
            displayedDialogue = dialogue;

            if (IsDialogueEmpty())
            {
                DisableContainer();
            }
            else
            {
                SetupDialogueBox();
                EnableContainer();
                RebuildDialogueBox();
            }
        }

        public Vector2 GetScreenSpacePosition(Vector3 worldSpacePosition)
        {
            try
            {
                return Camera.main.WorldToScreenPoint(worldSpacePosition);
            }
            catch
            {
                Debug.LogError("MainCamera not found!");

                return Vector2.zero;
            }
        }

        public void DisableContainer()
        {
            containerObject.SetActive(false);
        }

        #endregion

        #region Private Methods

        private bool IsDialogueEmpty()
        {
            if (displayedDialogue.text == "")
            {
                return true;
            }

            return false;
        }

        private void EnableContainer()
        {
            containerObject.SetActive(true);
        }

        private void RebuildDialogueBox()
        {
            foreach (RectTransform rt in LayoutGroupsToRefresh)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
            }
        }

        private void SetupDialogueBox()
        {
            narratorTMP.text = displayedDialogue.narrator;
            dialogueTMP.text = displayedDialogue.text;
        }

        private void UpdateDialogueBox()
        {
            boxRectTransform.position = GetScreenSpacePosition(displayedDialogue.worldSpacePosition);
            boxRectTransform.pivot = new Vector2(displayedDialogue.pivotXPosition, displayedDialogue.pivotYPosition);
        }

        private void GetReferences()
        {
            boxRectTransform = GetComponent<RectTransform>();
        }

        #endregion
    }
}