using System;
using UnityEngine;
using Utils.Triggers;

namespace DialogueSystem
{
    [CreateAssetMenu(menuName = "Dialogue/Create Current Dialogue", fileName = "CurrentDialogue")]
    public class CurrentDialogueData : ScriptableObject
    {
        #region Events

        public event Action OnClosed;

        #endregion

        #region Serialized Fields

        [SerializeField]
        public TriggerEvent DialogueUpdatedTrigger;

        #endregion

        public Dialogue dialogue;

        #region Public Methods

        public void UpdateData(Dialogue dialogue)
        {
            this.dialogue = dialogue;
            InvokeDialogueUpdated();
        }

        public void CloseDialogue()
        {
            OnClosed?.Invoke();
        }

        #endregion

        #region Private Methods

        private void InvokeDialogueUpdated()
        {
            DialogueUpdatedTrigger.InvokeTrigger();
        }

        #endregion
    }
}