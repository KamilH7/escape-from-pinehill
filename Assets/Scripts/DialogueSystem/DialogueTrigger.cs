using UnityEngine;
using Utils;

namespace DialogueSystem
{
    public class DialogueTrigger : MonoBehaviour
    {
        #region Serialized Fields

        [Header("References"), SerializeField]
        private PlayerCollisionDetector playerDetector;
        [SerializeField]
        private CurrentDialogueData currentDialogueData;

        [Header("Dialogue Settings"), SerializeField]
        private Dialogue activeDialogue;

        [SerializeField]
        private bool oneTime;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            AssignCallback();
        }

        private void OnDestroy()
        {
            UnassignCallback();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(transform.position, 0.5f);
            Gizmos.color = Color.white;
        }

        #endregion

        #region Public Methods

        public void UpdateDialogue(Dialogue newDialogue)
        {
            activeDialogue = newDialogue;
        }

        #endregion

        #region Private Methods

        private void PlayerEnter()
        {
            activeDialogue.worldSpacePosition = transform.position;
            currentDialogueData.UpdateData(activeDialogue);
        }

        private void PlayerExit()
        {
            currentDialogueData.UpdateData(new Dialogue());

            if (oneTime)
            {
                Destroy(gameObject);
            }
        }

        private void AssignCallback()
        {
            playerDetector.OnPlayerEnter += PlayerEnter;
            playerDetector.OnPlayerExit += PlayerExit;
        }

        private void UnassignCallback()
        {
            playerDetector.OnPlayerEnter -= PlayerEnter;
            playerDetector.OnPlayerExit -= PlayerExit;
        }

        #endregion
    }
}