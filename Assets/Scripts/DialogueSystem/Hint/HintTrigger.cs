﻿using UnityEngine;
using Utils;
using Utils.DataVariables;

namespace DialogueSystem.Hint
{
    public class HintTrigger : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private HintsData hintsData;
        [SerializeField]
        private Vector3Variable playerPosition;

        [SerializeField]
        private CurrentDialogueData dialogueData;
        [SerializeField]
        private string key;

        #endregion

        #region Unity Callbacks

        private void OnTriggerStay(Collider other)
        {
            if (other.IsPlayer())
            {
                Dialogue dialogue = new Dialogue();
                dialogue.worldSpacePosition = playerPosition.Value;
                dialogue.text = hintsData.GetHint(key);
                dialogueData.UpdateData(dialogue);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            dialogueData.CloseDialogue();
        }

        #endregion
    }
}