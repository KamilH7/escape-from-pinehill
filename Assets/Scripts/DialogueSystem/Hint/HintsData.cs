﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace DialogueSystem.Hint
{
    [CreateAssetMenu(menuName = "Dialogue/Create HintsData", fileName = "HintsData", order = 0)]
    public class HintsData : SerializedScriptableObject
    {
        #region Private Fields

        [OdinSerialize]
        private Dictionary<string, string> hintByKey;

        #endregion

        #region Public Methods

        public string GetHint(string key)
        {
            return hintByKey.TryGetValue(key, out string value) ? value : "";
        }

        #endregion
    }
}