using Movement.Data;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement
{
    public class PlayerMovement : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private MovementConfiguration movementConfiguration;
        [SerializeField]
        private InputActionReference movementActionReferences;
        [SerializeField]
        private float cameraAngle;

        #endregion

        #region Private Fields

        [ShowInInspector]
        private Vector3 directionToMove;

        private Rigidbody playerRigidBody;
        private float rotationAngle;
        private Vector2 input;
        private Camera mainCamera;

        #endregion

        #region Public Properties

        public float CameraAngle { get => cameraAngle; set => cameraAngle = value; }

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            GetReferences();
            movementConfiguration.Initialize();
        }

        private void Update()
        {
            GetInput();

            if (!IsAnyInput())
            {
                return;
            }

            DesignateDirection();
            RotatePlayer();
        }

        private void FixedUpdate()
        {
            ApplyMovement();
        }

        #endregion

        #region Private Methods

        private void ApplyMovement()
        {
            if (IsAnyInput())
            {
                movementConfiguration.Accelerate();
            }
            else
            {
                movementConfiguration.Decelerate();
            }

            playerRigidBody.velocity = directionToMove * movementConfiguration.CurrentSpeed;
        }

        private void DesignateDirection()
        {
            Vector3 camForward = Quaternion.Euler(0f, cameraAngle, 0f) * Vector3.ProjectOnPlane(mainCamera.transform.forward, Vector3.up).normalized;
            Vector3 camRight = Quaternion.Euler(0f, 90f, 0f) * camForward;
            directionToMove = input.x * camRight + input.y * camForward;
        }

        private void RotatePlayer()
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(directionToMove), movementConfiguration.TurnSpeed * Time.deltaTime);
        }

        private bool IsAnyInput()
        {
            return input.magnitude >= double.Epsilon;
        }

        private void GetInput()
        {
            input = movementActionReferences.action.ReadValue<Vector2>();
        }

        private void GetReferences()
        {
            playerRigidBody = GetComponent<Rigidbody>();
            mainCamera = Camera.main;
        }

        #endregion
    }
}