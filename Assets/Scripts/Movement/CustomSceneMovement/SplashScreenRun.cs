using UnityEngine;

namespace Movement.CustomSceneMovement
{
    public class SplashScreenRun : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Animator animator;

        #endregion

        #region Private Fields

        private string animationRunName = "Run";

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            animator.SetBool(animationRunName, true);
        }

        #endregion
    }
}