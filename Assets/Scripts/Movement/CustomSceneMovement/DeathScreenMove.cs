using UnityEngine;

namespace Movement.CustomSceneMovement
{
    public class DeathScreenMove : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Animator animator;

        #endregion

        #region Private Fields

        private string animationLoseName = "Lose";

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            animator.SetTrigger(animationLoseName);
        }

        #endregion
    }
}