using UnityEngine;

namespace Movement.CustomSceneMovement
{
    public class WinScreen : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Animator animator;

        #endregion

        #region Private Fields

        private string triggerName = "Win";

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            animator.SetBool(triggerName, true);
        }

        #endregion
    }
}