using System.Collections.Generic;
using Combat;
using Movement.Data;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Movement
{
    public class PlayerAnimationController : SerializedMonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private MovementConfiguration movementConfiguration;

        #endregion

        #region Private Fields

        [OdinSerialize]
        private Dictionary<PlayerState, string> triggerByPlayerState;

        private CrouchController crouchController;
        private Shooter shooter;

        private PlayerState currentPlayerState;

        private string currentParameter;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            crouchController = GetComponent<CrouchController>();
            shooter = GetComponent<Shooter>();
            AssignCallback();
        }

        private void Update()
        {
            GetPlayerState();
            DesignateAnimation();
        }

        #endregion

        #region Private Methods

        private void AssignCallback()
        {
            shooter.OnShooted += ShootAnimation;
        }

        private void ShootAnimation()
        {
            if (triggerByPlayerState.TryGetValue(PlayerState.Attacking, out string parameter))
            {
                animator.SetTrigger(parameter);
            }
        }

        private void DesignateAnimation()
        {
            if (currentParameter != "")
            {
                animator.SetBool(currentParameter, false);
            }

            if (triggerByPlayerState.TryGetValue(currentPlayerState, out string parameter))
            {
                animator.SetBool(parameter, true);
                currentParameter = parameter;
            }
        }

        private void GetPlayerState()
        {
            currentPlayerState = movementConfiguration.GetPlayerState();
        }

        #endregion
    }
}