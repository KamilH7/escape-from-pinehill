using System;
using GameLoop.Data;
using Movement.Data;
using SceneManagement.SceneData;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Movement
{
    public class CrouchController : MonoBehaviour
    {
        #region Events

        public event Action OnCrouch;
        public event Action OnStopCrouching;

        #endregion

        #region Serialized Fields

        [SerializeField]
        private MovementConfiguration movementConfiguration;
        [SerializeField]
        private InputActionReference crouchActionReferences;
        [SerializeField]
        private RoomsData roomsData;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            AssignCallback();
        }

        #endregion

        #region Private Methods

        private void TryCrouching(Room room)
        {
            if (roomsData.GetRoomType(room) == RoomType.Stealth)
            {
                StartCrouch();
            }
            else
            {
                CancelCrouch();
            }
        }

        private void StartCrouch()
        {
            OnCrouch?.Invoke();
            movementConfiguration.SetPlayerSpeedByState(PlayerState.Crouching);
        }

        private void CancelCrouch()
        {
            OnStopCrouching?.Invoke();
            movementConfiguration.SetPlayerSpeedByState(PlayerState.Walking);
        }

        private void AssignCallback()
        {
            GameEvents.OnLevelChanged += TryCrouching;
        }

        #endregion
    }
}