﻿namespace Movement.Data
{
    public enum PlayerState
    {
        Walking,
        Crouching,
        Attacking,
        Losing,
        Idle
    }
}