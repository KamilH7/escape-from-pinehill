using System.Collections.Generic;
using Cinemachine;
using GameLoop.Data;
using SceneManagement.SceneData;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Movement.Data
{
    public class CameraConfigHolder : SerializedMonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private PlayerMovement playerMovement;

        #endregion

        #region Private Fields

        [OdinSerialize]
        private Dictionary<Room, CinemachineVirtualCamera> cameraByRoom = new Dictionary<Room, CinemachineVirtualCamera>();
        [OdinSerialize]
        private Dictionary<Room, float> cameraAngleValueByRoom = new Dictionary<Room, float>();

        private CinemachineVirtualCamera currentCamera;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            GameEvents.OnLevelChanged += ChangeCamera;
        }

        #endregion

        #region Private Methods

        private void ChangeCamera(Room room)
        {
            if (currentCamera)
            {
                currentCamera.gameObject.SetActive(false);
            }

            if (cameraByRoom.TryGetValue(room, out CinemachineVirtualCamera camera))
            {
                currentCamera = camera;
                camera.gameObject.SetActive(true);
                playerMovement.CameraAngle = cameraAngleValueByRoom[room];
            }
        }

        #endregion
    }
}