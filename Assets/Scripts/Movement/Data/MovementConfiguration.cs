using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using UnityEngine;

namespace Movement.Data
{
    [CreateAssetMenu(fileName = "MovementConfiguration", menuName = "Movement/Create Movement Configuration")]
    public class MovementConfiguration : SerializedScriptableObject
    {
        #region Serialized Fields

        [SerializeField]
        private float initialSpeed;
        [SerializeField]
        private float maximumSpeed = 5f;
        [SerializeField]
        private float turnSpeed = 30f;
        [SerializeField]
        private float accelerationRate;
        [SerializeField]
        private float decelerationRate;
        [SerializeField]
        private float timeToCrouch = 0.1f;

        #endregion

        #region Private Fields

        [OdinSerialize]
        private Dictionary<PlayerState, float> speedByState;

        private float currentSpeed;
        private Dictionary<float, PlayerState> stateBySpeedThreshold = new Dictionary<float, PlayerState>();

        #endregion

        #region Public Properties

        public float CurrentSpeed => currentSpeed;
        public float TurnSpeed => turnSpeed;

        #endregion

        #region Public Methods

        public void Initialize()
        {
            speedByState.ForEach(keyValuePair => stateBySpeedThreshold.Add(keyValuePair.Value, keyValuePair.Key));
        }

        public void Accelerate()
        {
            SetPlayerSpeed(currentSpeed + accelerationRate * Time.deltaTime);
        }

        public void Decelerate()
        {
            SetPlayerSpeed(currentSpeed - decelerationRate * Time.deltaTime);
        }

        public void SetPlayerSpeedByState(PlayerState state)
        {
            if (speedByState.TryGetValue(state, out float speed))
            {
                DOTween.To(() => maximumSpeed, x => maximumSpeed = x, speed, timeToCrouch);
            }
        }

        public PlayerState GetPlayerState()
        {
            if (currentSpeed <= 0.2f)
            {
                return PlayerState.Idle;
            }

            return stateBySpeedThreshold.TryGetValue(maximumSpeed, out PlayerState currentState) ? currentState : PlayerState.Idle;
        }

        #endregion

        #region Private Methods

        private void SetPlayerSpeed(float speed)
        {
            currentSpeed = Mathf.Clamp(speed, initialSpeed, maximumSpeed);
        }

        #endregion
    }
}