﻿using System;
using SceneManagement.SceneData;

namespace GameLoop.Data
{
    public static class GameEvents
    {
        #region Events

        public static event Action OnPlayerDied;
        public static event Action OnNextLevelPassed;
        public static event Action OnPlayerWon;
        public static event Action<Room> OnLevelChanged;
        public static event Action<Room> OnDevelopersLevelLoaded;

        #endregion

        #region Public Methods

        public static void PlayerDied()
        {
            OnPlayerDied?.Invoke();
        }

        public static void LevelPassed()
        {
            OnNextLevelPassed?.Invoke();
        }

        public static void PlayerWon()
        {
            OnPlayerWon?.Invoke();
        }

        public static void LoadDeveloperLevel(Room levelName)
        {
            OnDevelopersLevelLoaded.Invoke(levelName);
        }

        public static void NextLevelLoaded(Room levelName)
        {
            OnLevelChanged.Invoke(levelName);
        }

        #endregion
    }
}