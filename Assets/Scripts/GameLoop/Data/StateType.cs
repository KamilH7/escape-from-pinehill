namespace GameLoop.Data
{
    public enum StateType
    {
        SplashScreen,
        Pause,
        Game,
        Death,
        Credits
    }
}