﻿using GameLoop.Data;
using SceneManagement.SceneData;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameLoop.Mock
{
    public class Mock : MonoBehaviour
    {
        #region Private Methods

        [Button]
        private void Die()
        {
            GameEvents.PlayerDied();
        }

        [Button]
        private void LoadNextRoom()
        {
            GameEvents.LevelPassed();
        }

        [Button]
        private void LoadLevel(Room room)
        {
            GameEvents.LoadDeveloperLevel(room);
        }

        [Button]
        private void Win()
        {
            GameEvents.PlayerWon();
        }

        #endregion
    }
}