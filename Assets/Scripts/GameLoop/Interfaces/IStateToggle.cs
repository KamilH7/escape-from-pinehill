using GameLoop.Data;

namespace GameLoop.Interfaces
{
    public interface IStateToggle
    {
        #region Public Methods

        public void ToggleState(StateType stateType);

        #endregion
    }
}