﻿using GameLoop.Data;
using UI.Data;

namespace GameLoop.States
{
    public class DeathState : AState
    {
        #region Private Fields

        private UIEvents uiEvents;

        #endregion

        #region Public Properties

        public override StateType Type => StateType.Death;

        #endregion

        private UIEventType UIEventType => UIEventType.DeathConfirmation;

        #region Constructors

        public DeathState(UIEvents uiEvents)
        {
            this.uiEvents = uiEvents;
        }

        #endregion

        #region Public Methods

        public override void OnEnter()
        {
            uiEvents.AddListener(UIEventType, ToggleState);
        }

        public override void OnUpdate()
        {
        }

        public override void OnExit()
        {
            uiEvents.RemoveListener(UIEventType, ToggleState);
        }

        #endregion

        #region Private Methods

        private void ToggleState()
        {
            stateToggle.ToggleState(StateType.Game);
        }

        #endregion
    }
}