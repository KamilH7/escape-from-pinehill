﻿using GameLoop.Data;

namespace GameLoop.States
{
    public class CreditsState : AState
    {
        #region Public Properties

        public override StateType Type => StateType.Credits;

        #endregion

        #region Public Methods

        public override void OnEnter()
        {
        }

        public override void OnUpdate()
        {
        }

        public override void OnExit()
        {
        }

        #endregion
    }
}