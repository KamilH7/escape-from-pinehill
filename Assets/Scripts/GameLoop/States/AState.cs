﻿using GameLoop.Data;
using GameLoop.Interfaces;

namespace GameLoop.States
{
    public abstract class AState
    {
        #region Private Fields

        protected IStateToggle stateToggle;

        #endregion

        #region Public Properties

        public abstract StateType Type { get; }

        #endregion

        #region Public Methods

        public AState Construct(IStateToggle stateToggle)
        {
            this.stateToggle = stateToggle;

            return this;
        }

        public abstract void OnEnter();
        public abstract void OnUpdate();
        public abstract void OnExit();

        #endregion
    }
}