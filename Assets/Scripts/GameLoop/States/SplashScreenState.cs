﻿using GameLoop.Data;
using UnityEngine;

namespace GameLoop.States
{
    public class SplashScreenState : AState
    {
        #region Private Fields

        private float timer;

        #endregion

        #region Constants

        private const float Duration = 2f;

        #endregion

        #region Public Properties

        public override StateType Type => StateType.SplashScreen;

        #endregion

        #region Public Methods

        public override void OnUpdate()
        {
            timer += Time.deltaTime;

            if (timer >= Duration)
            {
                Exit();
            }
        }

        public override void OnEnter()
        {
            timer = 0;
        }

        public override void OnExit()
        {
        }

        #endregion

        #region Private Methods

        private void Exit()
        {
            stateToggle.ToggleState(StateType.Game);
        }

        #endregion
    }
}