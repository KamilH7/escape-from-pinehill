﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLoop.Data;
using Inventory.Data;
using SceneManagement.SceneChangers;
using SceneManagement.SceneData;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;
using Random = UnityEngine.Random;

namespace GameLoop.States
{
    public class GameState : AState
    {
        #region Private Fields

        private Animator playerAnimator;
        private InputActionReference movementInputAction;
        private RoomsData roomsData;
        private PlayerInventory playerInventory;
        private ISceneChanger<Room> roomChanger;

        private List<Room> currentRooms;

        private int currentRoomIndex;

        private bool isDying;

        #endregion

        #region Public Properties

        public override StateType Type => StateType.Game;

        #endregion

        #region Constructors

        public GameState(InputActionReference movementInputAction, ISceneChanger<Room> roomChanger, RoomsData roomsData, PlayerInventory playerInventory, Animator playerAnimator)
        {
            this.movementInputAction = movementInputAction;
            this.roomChanger = roomChanger;
            this.roomsData = roomsData;
            this.playerInventory = playerInventory;
            this.playerAnimator = playerAnimator;
        }

        #endregion

        #region Public Methods

        public override void OnEnter()
        {
            movementInputAction.asset.Enable();
            AssignCallback();
            GenerateRoomSequence();
            LoadRoom();
        }

        public override void OnUpdate()
        {
        }

        public override void OnExit()
        {
            UnloadRooms();
            movementInputAction.asset.Disable();
            GameEvents.OnPlayerDied -= Die;
            GameEvents.OnNextLevelPassed -= LoadNextLevel;
            GameEvents.OnPlayerWon -= Win;
            // GameEvents.OnDevelopersLevelLoaded -= DevelopersTool;
        }

        #endregion

        #region Private Methods

        private void AssignCallback()
        {
            GameEvents.OnPlayerDied += Die;
            GameEvents.OnNextLevelPassed += LoadNextLevel;
            // GameEvents.OnDevelopersLevelLoaded += DevelopersTool;
            GameEvents.OnPlayerWon += Win;
        }

        private void Win()
        {
            stateToggle.ToggleState(StateType.Credits);
        }

        private void DevelopersTool(Room roomName)
        {
            // UnloadRooms();
            // GameEvents.NextLevelLoaded(roomName);
            // roomChanger.TryLoadRoom(roomName);
            // currentRooms.Add(roomName);
        }

        private void LoadRoom()
        {
            roomChanger.TryLoadRoom(currentRooms[currentRoomIndex]);
            GameEvents.NextLevelLoaded(currentRooms[currentRoomIndex]);
        }

        private void GenerateRoomSequence()
        {
            currentRooms = new List<Room>();
            int roomAmount = Random.Range(3, roomsData.RoomsAmount);

            foreach (RoomType roomType in Enum.GetValues(typeof(RoomType)))
            {
                currentRooms.AddRange(roomsData.GetRandomRooms(roomAmount * roomsData.RoomPercent(roomType), roomType));
            }

            System.Random random = new System.Random();
            currentRooms = currentRooms.OrderBy(x => random.Next()).ToList();
            currentRooms.Add(Room.Boss);
            currentRooms.ForEach(room => Debug.Log(room));
            Debug.Log("LOADED");
        }

        private void UnloadRooms()
        {
            currentRooms?.ForEach(room => roomChanger.TryUnloadRoom(room));
        }

        private void LoadNextLevel()
        {
            if (currentRoomIndex + 1 >= currentRooms.Count)
            {
                GameEvents.PlayerWon();

                return;
            }

            roomChanger.TryUnloadRoom(currentRooms[currentRoomIndex]);
            CountUpLevelIndex();
            LoadRoom();
        }

        private void CountUpLevelIndex()
        {
            currentRoomIndex++;
        }

        private void Die()
        {
            if (isDying)
            {
                return;
            }

            isDying = true;
            new CustomTimer(2f).OnTimerStopped += CompleteDying;
            playerAnimator.SetTrigger("Lose");
            movementInputAction.asset.Disable();
        }

        private void CompleteDying()
        {
            isDying = false;

            if (!playerInventory.HasIdCards)
            {
                stateToggle.ToggleState(StateType.Death);

                return;
            }

            playerInventory.UseIdCard();
            ReloadCurrentRoom();
        }

        private void ReloadCurrentRoom()
        {
            roomChanger.TryUnloadRoom(currentRooms[currentRoomIndex]);
            LoadRoom();
        }

        #endregion
    }
}