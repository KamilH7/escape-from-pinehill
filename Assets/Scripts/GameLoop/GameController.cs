﻿using System.Collections.Generic;
using GameLoop.Data;
using GameLoop.States;
using Inventory.Data;
using SceneManagement.SceneChangers;
using SceneManagement.SceneData;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UI.Data;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameLoop
{
    public class GameController : SerializedMonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private InputActionReference closeWindowInputAction;
        [SerializeField]
        private InputActionReference movementInputAction;
        [SerializeField]
        private UIEvents uiEvents;
        [SerializeField]
        private RoomsData roomsData;
        [SerializeField]
        private SceneData scenesData;
        [SerializeField]
        private PlayerInventory playerInventory;
        [SerializeField]
        private Animator playerAnimator;

        #endregion

        #region Private Fields

        private ISceneChanger<Room> roomChanger;
        private ISceneChanger<Scene> sceneChanger;

        [OdinSerialize]
        private Dictionary<StateType, List<Scene>> scenesByState;

        private StateMachine stateMachine;

        #endregion

        #region Constants

        private const StateType InitialState = StateType.SplashScreen;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            AssignCallback();
        }

        private void Start()
        {
            EnableInput();
            GetReferences();
            InitializeStates();
        }

        private void Update()
        {
            stateMachine.Tick();
        }

        #endregion

        #region Private Methods

        private void GetReferences()
        {
            sceneChanger = GetComponent<ISceneChanger<Scene>>();
            roomChanger = GetComponent<ISceneChanger<Room>>();
        }

        private void InitializeStates()
        {
            stateMachine = new StateMachine(scenesByState, sceneChanger);

            RegisterState(new SplashScreenState());
            RegisterState(new GameState(movementInputAction, roomChanger, roomsData, playerInventory, playerAnimator));
            RegisterState(new DeathState(uiEvents));
            RegisterState(new CreditsState());

            stateMachine.ToggleState(InitialState);
        }

        private void RegisterState(AState state)
        {
            state.Construct(stateMachine);
            stateMachine.AddState(state);
        }
        private void AssignCallback()
        {
            closeWindowInputAction.action.started += CloseWindow;
        }

        private void EnableInput()
        {
            closeWindowInputAction.asset.Enable();
        }

        private void CloseWindow(InputAction.CallbackContext context)
        {
            Application.Quit();
        }
        #endregion
    }
}