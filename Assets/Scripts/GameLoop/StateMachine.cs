﻿using System.Collections.Generic;
using GameLoop.Data;
using GameLoop.Interfaces;
using GameLoop.States;
using SceneManagement.SceneChangers;
using SceneManagement.SceneData;

namespace GameLoop
{
    public class StateMachine : IStateToggle
    {
        #region Private Fields

        private Dictionary<StateType, AState> stateByName = new Dictionary<StateType, AState>();
        private Dictionary<StateType, List<Scene>> sceneByState;

        private AState currentState;
        private List<Scene> currentScenes;
        private ISceneChanger<Scene> sceneChanger;

        #endregion

        #region Constructors

        public StateMachine(Dictionary<StateType, List<Scene>> sceneByState, ISceneChanger<Scene> sceneChanger)
        {
            this.sceneByState = sceneByState;
            this.sceneChanger = sceneChanger;
        }

        #endregion

        #region Public Methods

        public void AddState(AState state)
        {
            stateByName.Add(state.Type, state);
        }

        public void ToggleState(StateType nextStateType)
        {
            if (stateByName.TryGetValue(nextStateType, out AState nextState))
            {
                ToggleState(nextState);
                ToggleScenes(nextStateType);
            }
        }

        public void Tick()
        {
            currentState?.OnUpdate();
        }

        #endregion

        #region Private Methods

        private void ToggleScenes(StateType nextStateType)
        {
            if (sceneByState.TryGetValue(nextStateType, out List<Scene> scenes))
            {
                currentScenes?.ForEach(room => sceneChanger.TryUnloadRoom(room));
                scenes?.ForEach(room => sceneChanger.TryLoadRoom(room));
                currentScenes = scenes;
            }
        }

        private void ToggleState(AState state)
        {
            currentState?.OnExit();
            currentState = state;
            state?.OnEnter();
        }

        #endregion
    }
}