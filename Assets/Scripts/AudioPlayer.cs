using FMODUnity;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(StudioEventEmitter))]
public class AudioPlayer : MonoBehaviour
{
    #region Serialized Fields

    [SerializeField]
    private StudioEventEmitter studioEventEmitter;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        studioEventEmitter = GetComponent<StudioEventEmitter>();
    }

    #endregion

    #region Public Methods

    [Button]
    public void Play()
    {
        studioEventEmitter.Play();
    }

    #endregion
}