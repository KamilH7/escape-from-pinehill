using UnityEngine;
using Utils.DataVariables;

namespace Utils
{
    public class PlayerInitializer : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Vector3Variable playerInitialPosition;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            playerInitialPosition.Value = transform.position;
        }

        #endregion
    }
}