using GameLoop.Data;
using NPC;
using NPC.States;
using UnityEngine;

namespace Utils
{
    public class LevelEndingTriggerFight : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private NpcAttack[] AttackNpcs;

        #endregion

        #region Unity Callbacks

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer() && AllEnemiesDead())
            {
                GameEvents.LevelPassed();
            }
        }

        #endregion

        #region Private Methods

        private bool AllEnemiesDead()
        {
            foreach (NpcAttack npc in AttackNpcs)
            {
                if (npc.IsAlive)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }
}