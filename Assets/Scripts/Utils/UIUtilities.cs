﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Utils
{
    /// <summary>
    ///     Class responsible for:
    ///     - Providing methods to facilitate work with the UI.
    /// </summary>
    public static class UIUtilities
    {
        #region Public Methods

        [MenuItem("DreamStorm/UI Utilities/Anchors to Corners %[")]
        public static void AnchorsToCorners()
        {
            Transform[] selectedTransforms = Selection.transforms;
            Undo.RegisterCompleteObjectUndo(selectedTransforms, "Anchors to Corners");

            foreach (Transform transform in selectedTransforms)
            {
                RectTransform t = transform as RectTransform;
                RectTransform pt = Selection.activeTransform.parent as RectTransform;

                if (t == null || pt == null)
                {
                    return;
                }

                Vector2 newAnchorsMin = new Vector2(t.anchorMin.x + t.offsetMin.x / pt.rect.width, t.anchorMin.y + t.offsetMin.y / pt.rect.height);
                Vector2 newAnchorsMax = new Vector2(t.anchorMax.x + t.offsetMax.x / pt.rect.width, t.anchorMax.y + t.offsetMax.y / pt.rect.height);
                t.anchorMin = newAnchorsMin;
                t.anchorMax = newAnchorsMax;
                t.offsetMin = t.offsetMax = new Vector2(0, 0);
            }
        }

        [MenuItem("DreamStorm//UI Utilities/Corners to Anchors %]")]
        public static void CornersToAnchors()
        {
            Transform[] selectedTransforms = Selection.transforms;
            Undo.RegisterCompleteObjectUndo(selectedTransforms, "Corners To Anchors");

            foreach (Transform transform in selectedTransforms)
            {
                RectTransform t = transform as RectTransform;

                if (t == null)
                {
                    return;
                }

                t.offsetMin = t.offsetMax = new Vector2(0, 0);
            }
        }

        [MenuItem("DreamStorm//UI Utilities/Mirror Horizontally Around Anchors %;")]
        public static void MirrorHorizontallyAnchors()
        {
            MirrorHorizontally(false);
        }

        [MenuItem("DreamStorm//UI Utilities/Mirror Horizontally Around Parent Center %:")]
        public static void MirrorHorizontallyParent()
        {
            MirrorHorizontally(true);
        }

        [MenuItem("DreamStorm//UI Utilities/Mirror Vertically Around Anchors %'")]
        public static void MirrorVerticallyAnchors()
        {
            MirrorVertically(false);
        }

        [MenuItem("DreamStorm//UI Utilities/Mirror Vertically Around Parent Center %\"")]
        public static void MirrorVerticallyParent()
        {
            MirrorVertically(true);
        }

        #endregion

        #region Private Methods

        private static void MirrorHorizontally(bool mirrorAnchors)
        {
            Transform[] selectedTransforms = Selection.transforms;
            Undo.RegisterCompleteObjectUndo(selectedTransforms, "Mirror horizontally");

            foreach (Transform transform in selectedTransforms)
            {
                RectTransform t = transform as RectTransform;
                RectTransform pt = Selection.activeTransform.parent as RectTransform;

                if (t == null || pt == null)
                {
                    return;
                }

                if (mirrorAnchors)
                {
                    Vector2 oldAnchorMin = t.anchorMin;
                    t.anchorMin = new Vector2(1 - t.anchorMax.x, t.anchorMin.y);
                    t.anchorMax = new Vector2(1 - oldAnchorMin.x, t.anchorMax.y);
                }

                Vector2 oldOffsetMin = t.offsetMin;
                t.offsetMin = new Vector2(-t.offsetMax.x, t.offsetMin.y);
                t.offsetMax = new Vector2(-oldOffsetMin.x, t.offsetMax.y);

                t.localScale = new Vector3(-t.localScale.x, t.localScale.y, t.localScale.z);
            }
        }

        private static void MirrorVertically(bool mirrorAnchors)
        {
            Transform[] selectedTransforms = Selection.transforms;
            Undo.RegisterCompleteObjectUndo(selectedTransforms, "Mirror vertically");

            foreach (Transform transform in selectedTransforms)
            {
                RectTransform t = transform as RectTransform;
                RectTransform pt = Selection.activeTransform.parent as RectTransform;

                if (t == null || pt == null)
                {
                    return;
                }

                if (mirrorAnchors)
                {
                    Vector2 oldAnchorMin = t.anchorMin;
                    t.anchorMin = new Vector2(t.anchorMin.x, 1 - t.anchorMax.y);
                    t.anchorMax = new Vector2(t.anchorMax.x, 1 - oldAnchorMin.y);
                }

                Vector2 oldOffsetMin = t.offsetMin;
                t.offsetMin = new Vector2(t.offsetMin.x, -t.offsetMax.y);
                t.offsetMax = new Vector2(t.offsetMax.x, -oldOffsetMin.y);

                t.localScale = new Vector3(t.localScale.x, -t.localScale.y, t.localScale.z);
            }
        }

        #endregion
    }
}

#endif