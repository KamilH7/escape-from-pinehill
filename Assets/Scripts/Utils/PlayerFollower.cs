using UnityEngine;

namespace Utils
{
    public class PlayerFollower : MonoBehaviour
    {
        #region Unity Callbacks

        private void Awake()
        {
            transform.SetParent(null);
        }

        #endregion
    }
}