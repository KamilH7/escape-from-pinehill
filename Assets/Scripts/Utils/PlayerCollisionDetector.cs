using System;
using UnityEngine;

namespace Utils
{
    public class PlayerCollisionDetector : MonoBehaviour
    {
        #region Events

        public event Action OnPlayerEnter;
        public event Action OnPlayerExit;

        #endregion

        #region Unity Callbacks

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer())
            {
                OnPlayerEnter?.Invoke();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.IsPlayer())
            {
                OnPlayerExit?.Invoke();
            }
        }

        #endregion
    }
}