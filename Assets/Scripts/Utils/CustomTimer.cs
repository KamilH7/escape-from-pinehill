using System;
using DG.Tweening;

namespace Utils
{
    public class CustomTimer
    {
        #region Events

        public event Action OnTimerStopped;

        #endregion

        #region Constructors

        public CustomTimer(float seconds)
        {
            StartTimer(seconds);
        }

        #endregion

        #region Private Methods

        private void StartTimer(float seconds)
        {
            float currentTime = 0;
            DOTween.To(() => currentTime, x => currentTime = x, seconds, seconds).OnComplete(InvokeTimer);
        }

        private void InvokeTimer()
        {
            OnTimerStopped?.Invoke();
        }

        #endregion
    }
}