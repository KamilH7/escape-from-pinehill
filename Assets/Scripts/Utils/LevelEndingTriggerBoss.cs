using GameLoop.Data;
using NPC.Boss;
using UnityEngine;

namespace Utils
{
    public class LevelEndingTriggerBoss : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private BossHead[] bossHeads;

        #endregion

        #region Unity Callbacks

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer() && isBossDead())
            {
                GameEvents.PlayerWon();
            }
        }

        #endregion

        #region Private Methods

        private bool isBossDead()
        {
            foreach (BossHead head in bossHeads)
            {
                if (!head.isDead)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }
}