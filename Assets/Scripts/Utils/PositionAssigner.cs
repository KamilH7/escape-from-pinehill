﻿using UnityEngine;
using Utils.DataVariables;

namespace Utils
{
    public class PositionAssigner : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Vector3Variable playerPositionVariable;

        #endregion

        #region Unity Callbacks

        private void Update()
        {
            playerPositionVariable.Value = transform.position;
        }

        #endregion
    }
}