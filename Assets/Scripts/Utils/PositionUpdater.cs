using UnityEngine;
using Utils.DataVariables;

namespace Utils
{
    public class PositionUpdater : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Vector3Variable playerInitialPosition;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            playerInitialPosition.OnValueChanged += ChangePosition;
        }

        #endregion

        #region Private Methods

        private void ChangePosition()
        {
            transform.position = playerInitialPosition.Value;
        }

        #endregion
    }
}