using GameLoop.Data;
using UnityEngine;

namespace Utils
{
    public class LevelEndingTrigger : MonoBehaviour
    {
        #region Unity Callbacks

        private bool hasEntered;

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer() && !hasEntered)
            {
                hasEntered = true;
                GameEvents.LevelPassed();
            }
        }

        #endregion
    }
}