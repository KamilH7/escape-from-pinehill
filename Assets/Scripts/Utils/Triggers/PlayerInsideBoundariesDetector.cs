﻿using UnityEngine;

namespace Utils.Triggers
{
    public class PlayerInsideBoundariesDetector : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private TriggerEvent triggerEvent;

        #endregion

        #region Constants

        private const string PlayerTag = "Player";

        #endregion

        #region Unity Callbacks

        private void OnTriggerEnter(Collider other)
        {
            if (ShouldDetect(other))
            {
                triggerEvent.InvokeTrigger();
            }
        }

        #endregion

        #region Private Methods

        private bool ShouldDetect(Collider other) => other.CompareTag(PlayerTag);

        #endregion
    }
}