﻿using UnityEngine;

namespace Utils
{
    public static class ExtensionMethods
    {
        public static bool IsPlayer(this Component other)
        {
            return other.CompareTag("Player");
        }

        public static bool IsBullet(this Component other)
        {
            return other.CompareTag("Bullet");
        }

        public static bool IsEnemy(this Component other)
        {
            return other.CompareTag("Enemy");
        }
    }
}