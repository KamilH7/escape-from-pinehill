﻿using UnityEngine;

namespace Utils.DataVariables
{
    [CreateAssetMenu(menuName = "Variabels/Create StringVariable", fileName = "StringVariable", order = 0)]
    public class StringVariable : DataVariable<string>
    {
    }
}