﻿using SceneManagement.SceneData;
using UnityEngine;

namespace Utils.DataVariables
{
    [CreateAssetMenu(menuName = "DataVariable/Create Room Variable", fileName = "RoomVariable")]
    public class RoomVariable : DataVariable<Room>
    {
    }
}