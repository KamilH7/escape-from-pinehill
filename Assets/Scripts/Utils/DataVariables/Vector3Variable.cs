﻿using UnityEngine;

namespace Utils.DataVariables
{
    [CreateAssetMenu(menuName = "DataVariable/Create Vector3 Variable", fileName = "Vector3Variable")]
    public class Vector3Variable : DataVariable<Vector3>
    {
    }
}