﻿using System;
using UnityEngine;

namespace Utils.DataVariables
{
    public class DataVariable<T> : ScriptableObject
    {
        #region Events

        public event Action OnValueChanged;

        #endregion

        #region Private Fields

        private T value;

        #endregion

        #region Public Properties

        public T Value {
            get => value;
            set
            {
                this.value = value;
                OnValueChanged?.Invoke();
            }
        }

        #endregion
    }
}