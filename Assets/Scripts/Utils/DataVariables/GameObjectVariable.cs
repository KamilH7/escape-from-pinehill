﻿using UnityEngine;

namespace Utils.DataVariables
{
    [CreateAssetMenu(menuName = "DataVariables/Create GameObjectVariable", fileName = "GameObjectVariable", order = 0)]
    public class GameObjectVariable : DataVariable<GameObject>
    {
    }
}