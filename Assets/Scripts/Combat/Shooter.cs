using System;
using Inventory.Data;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Combat
{
    public class Shooter : MonoBehaviour
    {
        #region Events

        public event Action OnShooted;

        #endregion

        #region Serialized Fields

        [SerializeField]
        private GameObject bulletPrefab;
        [SerializeField]
        private Transform bulletInitPosition;
        [SerializeField]
        private InputActionReference shootingActionReference;
        [SerializeField]
        private PlayerInventory inventory;
        [SerializeField]
        private float fireCooldownDuration;

        #endregion

        #region Private Fields

        private float fireTimeStamp;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            AssignCallback();
        }

        private void Start()
        {
            EnableInput();
            SetNextAllowedTimeForFire(Time.time);
        }

        #endregion

        #region Private Methods

        private void TryFiring(InputAction.CallbackContext context)
        {
            if (!CanFire())
            {
                return;
            }
            
            OnShooted.Invoke();
            CreateBullet();
            SetNextAllowedTimeForFire(Time.time + fireCooldownDuration);
            inventory.UseBullet();
        }

        private bool CanFire() => inventory.HasBullets && CanFireDueToCooldown();

        private void SetNextAllowedTimeForFire(float time)
        {
            fireTimeStamp = time;
        }

        private bool CanFireDueToCooldown()
        {
            return fireTimeStamp < Time.time;
        }

        private void CreateBullet()
        {
            Instantiate(bulletPrefab, bulletInitPosition.position, transform.rotation);
        }

        private void AssignCallback()
        {
            shootingActionReference.action.started += TryFiring;
        }

        private void EnableInput()
        {
            shootingActionReference.asset.Enable();
        }

        #endregion
    }
}