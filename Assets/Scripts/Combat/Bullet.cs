using UnityEngine;
using Utils;
namespace Combat
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField]
        private float range;
        [SerializeField]
        private float speed;

        private Vector3 destination;

        private void Start()
        {
            destination = transform.position + transform.rotation * Vector3.forward * range;
        }

        private void Update()
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * speed);

            if (transform.position == destination)
            {
                Destroy(gameObject);
            }
        }
        
    }
}