﻿namespace SceneManagement.SceneChangers
{
    public interface ISceneChanger<T>
    {
        #region Public Methods

        public void TryLoadRoom(T type);
        public void TryUnloadRoom(T type);

        #endregion
    }
}