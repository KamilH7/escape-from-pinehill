using SceneManagement.SceneData;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneManagement.SceneChangers
{
    public abstract class ASceneChanger<T> : MonoBehaviour, ISceneChanger<T>
    {
        #region Serialized Fields

        [SerializeField]
        protected ASceneData<T> sceneData;

        #endregion

        #region Public Methods

        public void TryLoadRoom(T type)
        {
            int sceneID = GetSceneID(type);

            if (!IsSceneIndexValid(sceneID) || IsSceneLoaded(sceneID))
            {
                return;
            }

            LoadScene(sceneID);
        }

        public void TryUnloadRoom(T type)
        {
            int sceneID = GetSceneID(type);

            if (!IsSceneIndexValid(sceneID) || !IsSceneLoaded(sceneID))
            {
                return;
            }

            UnloadScene(sceneID);
        }

        #endregion

        #region Private Methods

        private void LoadScene(int sceneID)
        {
            SceneManager.LoadScene(sceneID, LoadSceneMode.Additive);
        }

        private void UnloadScene(int sceneId)
        {
            SceneManager.UnloadSceneAsync(sceneId);
        }

        private int GetSceneID(T type) => sceneData.GetSceneID(type);

        private bool IsSceneIndexValid(int index) => index != -1;

        private bool IsSceneLoaded(int sceneID) => SceneManager.GetSceneByBuildIndex(sceneID).isLoaded;

        #endregion
    }
}