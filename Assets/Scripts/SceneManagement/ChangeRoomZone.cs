using GameLoop.Data;
using UnityEngine;
using Utils;

namespace SceneManagement
{
    public class ChangeRoomZone : MonoBehaviour
    {
        #region Unity Callbacks

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer())
            {
                PlayerEnteredTrigger();
            }
        }

        #endregion

        #region Private Methods

        private void PlayerEnteredTrigger()
        {
            GameEvents.LevelPassed();
        }

        #endregion
    }
}