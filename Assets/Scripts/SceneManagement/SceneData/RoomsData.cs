﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Serialization;
using Sirenix.Utilities;
using UnityEngine;
using Random = System.Random;

namespace SceneManagement.SceneData
{
    [CreateAssetMenu(menuName = "UI/Room Data", fileName = "RoomData", order = 0)]
    public class RoomsData : ASceneData<Room>
    {
        #region Private Fields

        [OdinSerialize]
        private Dictionary<RoomType, List<Room>> roomsByType;
        [OdinSerialize]
        private Dictionary<RoomType, int> roomsPercentByType;

        #endregion

        #region Public Properties

        public int RoomsAmount => sceneIdBySceneName.Count;

        #endregion

        #region Public Methods

        public int RoomPercent(RoomType roomType)
        {
            return roomsPercentByType.TryGetValue(roomType, out int percent) ? percent : 0;
        }

        public List<Room> GetRandomRooms(int amount, RoomType roomType)
        {
            List<Room> resultRooms = new List<Room>();

            if (roomsByType.TryGetValue(roomType, out List<Room> rooms))
            {
                Random random = new Random();
                Enumerable.Range(0, rooms.Count).OrderBy(t => random.Next()).Take(Mathf.Min(amount, rooms.Count)).ForEach(index => resultRooms.Add(rooms[index]));
            }

            return resultRooms;
        }

        public RoomType GetRoomType(Room room)
        {
            return Enum.GetValues(typeof(RoomType)).Cast<RoomType>().FirstOrDefault(value => roomsByType[value].Contains(room));
        }

        #endregion
    }

    public enum RoomType
    {
        Fight,
        Stealth,
        Puzzle
    }
}