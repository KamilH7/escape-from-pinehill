using UnityEngine;

namespace SceneManagement.SceneData
{
    [CreateAssetMenu(menuName = "UI/Scene Data", fileName = "SceneData", order = 0)]
    public class SceneData : ASceneData<Scene>
    {
    }
}