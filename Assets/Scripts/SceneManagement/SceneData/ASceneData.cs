﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace SceneManagement.SceneData
{
    public abstract class ASceneData<T> : SerializedScriptableObject
    {
        #region Private Fields

        [OdinSerialize]
        protected Dictionary<T, int> sceneIdBySceneName;

        #endregion

        #region Public Methods

        public int GetSceneID(T scene)
        {
            return sceneIdBySceneName.TryGetValue(scene, out int id) ? id : -1;
        }

        #endregion
    }
}