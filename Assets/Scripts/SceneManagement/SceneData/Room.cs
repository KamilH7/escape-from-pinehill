using System;
using UnityEngine;

namespace SceneManagement.SceneData
{
    [Serializable]
    public enum Room
    {
        None = -1,
        Tiles,
        Levers,
        Exam,
        Boss,
        Stealth1,
        Stealth2,
        Stealth3,
        Fight1,
        Fight2
    }

    [SerializeField]
    public enum Scene
    {
        None = -1,
        MainScene,
        SplashScreen,
        UI,
        DevRoom,
        Death,
        Credits
    }
}