using System.Collections.Generic;
using Animation;
using Inventory.Data;
using NPC.Data;
using NPC.States;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace NPC
{
    public class PatrolTrigger : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Item requiredItem;
        [SerializeField]
        private PlayerInventory inventory;

        [SerializeField]
        private AnimationTrigger animtrigger;
        [SerializeField]
        private List<Point> newDestinationPoints;
        [SerializeField]
        private NpcPatrolling patrol;

        #endregion

        #region Private Fields

        private PlayerCollisionDetector playerDetector;
        private bool isActivated;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
            AssignCallbacks();
        }

        private void OnDestroy()
        {
            UnassignCallbacks();
        }

        #endregion

        #region Public Methods

        [Button]
        public void AddDestinationPointsToPatrol()
        {
            if (inventory.HasItem(requiredItem))
            {
                if (isActivated)
                {
                    return;
                }
                inventory.RemoveItem(requiredItem);
                TriggerAnimation();

                foreach (Point destinationPoint in newDestinationPoints)
                {
                    patrol.AddDestinationPoint(destinationPoint);
                }

                isActivated = true;
            }
        }

        #endregion

        #region Private Methods

        private void TriggerAnimation()
        {
            animtrigger.TriggerAnimations();
        }

        private void GetReferences()
        {
            playerDetector = GetComponentInChildren<PlayerCollisionDetector>();
        }

        private void AssignCallbacks()
        {
            playerDetector.OnPlayerEnter += AddDestinationPointsToPatrol;
        }

        private void UnassignCallbacks()
        {
            playerDetector.OnPlayerEnter -= AddDestinationPointsToPatrol;
        }

        #endregion
    }
}