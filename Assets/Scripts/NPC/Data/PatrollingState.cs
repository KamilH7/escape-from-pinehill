namespace NPC.Data
{
    public enum PatrollingState
    {
        Moving,
        Standing,
        Searching
    }
}