﻿using System;
using UnityEngine;

namespace NPC.Data
{
    [Serializable]
    public class Point
    {
        #region Serialized Fields

        [SerializeField]
        private float pointDuration;
        [SerializeField]
        private Transform pointTransform;

        #endregion

        #region Public Properties

        public float PointDuration => pointDuration;
        public Vector3 PointPosition => pointTransform.position;

        #endregion
    }
}