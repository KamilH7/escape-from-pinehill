using GameLoop.Data;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace NPC.Boss
{
    public class BossHead : MonoBehaviour
    {
        #region Private Fields

        private Animator headController;

        #endregion

        public bool isAttacking;
        public bool isDead;

        #region Unity Callbacks

        private void Start()
        {
            headController = GetComponent<Animator>();

            OffsetIdleAnimation();
        }

        private void OnTriggerStay(Collider other)
        {
            if (isAttacking)
            {
                if (other.IsPlayer())
                {
                    KillPlayer();
                }
                else if (other.IsBullet())
                {
                    KillHead();
                }
            }
            else
            {
                if (other.IsPlayer())
                {
                    Attack();
                }
            }
        }

        #endregion

        #region Private Methods

        [Button]
        private void SimulateBulletHit()
        {
            if (isAttacking)
            {
                KillHead();
            }
        }

        private void Attack()
        {
            if (GetClipInfo()[0].clip.name == "Idle")
            {
                headController.SetTrigger("Attack");
            }
        }

        [Button]
        private void KillHead()
        {
            headController.SetTrigger("Damage");
            isDead = true;
        }

        private void KillPlayer()
        {
            GameEvents.PlayerDied();
        }

        private void OffsetIdleAnimation()
        {
            float startTime = Random.Range(0.0f, GetClipInfo().Length);
            headController.Play("Idle", 0, startTime);
        }

        private AnimatorClipInfo[] GetClipInfo()
        {
            return headController.GetCurrentAnimatorClipInfo(0);
        }

        #endregion
    }
}