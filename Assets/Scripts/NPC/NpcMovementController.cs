﻿using NPC.Interfaces;
using UnityEngine;

namespace NPC
{
    public class NpcMovementController : MonoBehaviour, IMovable, IRotatable
    {
        #region Serialized Fields

        [SerializeField]
        private float rotationSpeed;
        [SerializeField]
        private float movingSpeed;

        #endregion

        #region Private Fields

        private Vector3 velocity;
        private Quaternion destinationAngle;

        private CharacterController characterController;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
        }

        private void Start()
        {
            destinationAngle = transform.rotation;
        }

        private void Update()
        {
            characterController.Move(velocity * (movingSpeed * Time.deltaTime));
            transform.rotation = Quaternion.Lerp(transform.rotation, destinationAngle, rotationSpeed * Time.deltaTime);
        }

        #endregion

        #region Public Methods

        public void Move(Vector3 vector)
        {
            velocity = vector;
        }

        public void Rotate(Quaternion quaternion)
        {
            Vector3 rotation = quaternion.eulerAngles;
            rotation = new Vector3(0, rotation.y, 0);
            quaternion = Quaternion.Euler(rotation);

            destinationAngle = quaternion;
        }

        #endregion

        #region Private Methods

        private void GetReferences()
        {
            characterController = GetComponent<CharacterController>();
        }

        #endregion
    }
}