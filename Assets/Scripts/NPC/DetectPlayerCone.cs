using System;
using UnityEngine;
using Utils;

namespace NPC
{
    public class DetectPlayerCone : MonoBehaviour
    {
        #region Events

        public event Action OnPlayerEntered;
        public event Action OnPlayerLeft;

        #endregion

        #region Serialized Fields

        [Range(0, 360), SerializeField]
        private int coneAngle;
        [SerializeField, Range(2, 20)]
        private float maxRaycastLength;
        [SerializeField, Range(10, 1000)]
        private int raycastDensity = 10;
        [SerializeField]
        private bool debugMode;

        [SerializeField]
        public bool playerInCone;

        #endregion

        #region Constants

        private const string EnemyLayer = "Enemy";

        #endregion

        #region Unity Callbacks

        private void Update()
        {
            HandlePlayerDetection();

            if (debugMode)
            {
                DrawRays();
            }
        }

        #endregion

        #region Public Methods

        public void DrawRays()
        {
            Color nextColor = new Color(0, 0, 0);
            float colorIncrementValue = 1 / (float) raycastDensity;

            foreach (Vector3 direction in GenerateConeRayDirections())
            {
                nextColor.r += colorIncrementValue;
                Debug.DrawRay(transform.position, direction * maxRaycastLength, nextColor);
            }
        }

        public Ray GetRay(Vector3 direction)
        {
            return new Ray(transform.position, direction);
        }

        public Vector3[] GenerateConeRayDirections()
        {
            Vector3[] diections = new Vector3[raycastDensity + 1];
            float angleIncrementRate = coneAngle / (float) raycastDensity;
            //set the starting cast angle to match the forward vector
            float castAngle = -transform.eulerAngles.y + 90;
            castAngle -= coneAngle / 2;

            for (int i = 0; i < diections.Length; i++)
            {
                diections[i] = CalculateCastDirection(castAngle);
                castAngle += angleIncrementRate;
            }

            return diections;
        }

        public bool IsPlayerHit(RaycastHit hitInfo)
        {
            if (hitInfo.collider == null)
            {
                return false;
            }

            if (hitInfo.collider.IsPlayer())
            {
                return true;
            }

            return false;
        }

        public RaycastHit Raycast(Ray ray)
        {
            RaycastHit raycastHit;
            Physics.Raycast(ray, out raycastHit, maxRaycastLength, ~GetEnemyLayerID());

            return raycastHit;
        }

        #endregion

        #region Private Methods

        private void HandlePlayerDetection()
        {
            bool playerDetectedInCone = IsPlayerWithinCone();

            if (PlayerEnteredCone(playerDetectedInCone))
            {
                playerInCone = true;
                OnPlayerEntered?.Invoke();
            }
            else if (PlayerLeftCone(playerDetectedInCone))
            {
                playerInCone = false;
                OnPlayerLeft?.Invoke();
            }
        }

        private bool PlayerEnteredCone(bool playerCurrentlyInCone)
        {
            if (!playerInCone && playerCurrentlyInCone)
            {
                return true;
            }

            return false;
        }

        private bool PlayerLeftCone(bool playerCurrentlyInCone)
        {
            if (playerInCone && !playerCurrentlyInCone)
            {
                return true;
            }

            return false;
        }

        private bool IsPlayerWithinCone()
        {
            foreach (Vector3 direction in GenerateConeRayDirections())
            {
                Ray ray = GetRay(direction);
                RaycastHit hitInfo = Raycast(ray);

                if (IsPlayerHit(hitInfo))
                {
                    return true;
                }
            }

            return false;
        }

        private Vector3 CalculateCastDirection(float angle)
        {
            float xCircumferencePoint = transform.position.x + maxRaycastLength * Mathf.Cos(EulerToRadian(angle));
            float zCircumferencePoint = transform.position.z + maxRaycastLength * Mathf.Sin(EulerToRadian(angle));

            Vector3 direction = new Vector3(xCircumferencePoint, transform.position.y, zCircumferencePoint) - transform.position;

            return direction.normalized;
        }

        private int GetEnemyLayerID()
        {
            return LayerMask.NameToLayer(EnemyLayer);
        }

        private float EulerToRadian(float angle)
        {
            return angle * Mathf.Deg2Rad;
        }

        #endregion
    }
}