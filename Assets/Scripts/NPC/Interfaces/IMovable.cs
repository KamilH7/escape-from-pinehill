﻿using UnityEngine;

namespace NPC.Interfaces
{
    public interface IMovable
    {
        #region Public Methods

        public void Move(Vector3 vector);

        #endregion
    }
}