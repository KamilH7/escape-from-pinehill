﻿using UnityEngine;

namespace NPC.Interfaces
{
    public interface IRotatable
    {
        #region Public Methods

        public void Rotate(Quaternion quaternion);

        #endregion
    }
}