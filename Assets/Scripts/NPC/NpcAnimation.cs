﻿using UnityEngine;

namespace NPC
{
    [RequireComponent(typeof(CharacterController))]
    public class NpcAnimation : MonoBehaviour
    {
        #region Private Fields

        private Animator animator;
        private Vector3 previousPosition;

        #endregion

        #region Constants

        private const string RunningParameter = "Run";

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            GetReferences();
        }

        private void Update()
        {
            animator.SetBool(RunningParameter, IsMoving());
            previousPosition = transform.position;
        }

        #endregion

        #region Private Methods

        private bool IsMoving() => previousPosition != transform.position;

        private void GetReferences()
        {
            animator = GetComponentInChildren<Animator>();
        }

        #endregion
    }
}