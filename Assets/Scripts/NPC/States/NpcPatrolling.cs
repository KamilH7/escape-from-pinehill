using System.Collections.Generic;
using NPC.Data;
using NPC.Interfaces;
using UnityEngine;

namespace NPC.States
{
    [RequireComponent(typeof(NpcMovementController))]
    public class NpcPatrolling : MonoBehaviour, IMovable, IRotatable
    {
        #region Serialized Fields

        [SerializeField]
        private List<Point> destinationPoints;
        [SerializeField]
        private float closeDistance = 0.2f;

        #endregion

        #region Private Fields

        private NpcMovementController npcMovementController;
        private PatrollingState patrollingState;

        private bool walkPointSet;
        private int currentDestPointIndex;

        private float waitingTime;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
        }

        private void Start()
        {
            InitializeValues();
        }

        private void Update()
        {
            PerformStateAction();
        }

        #endregion

        #region Public Methods

        public void Move(Vector3 vector)
        {
            npcMovementController.Move(vector);
        }

        public void Rotate(Quaternion quaternion)
        {
            npcMovementController.Rotate(quaternion);
        }

        public void AddDestinationPoint(Point destinationPoint)
        {
            destinationPoints.Add(destinationPoint);
        }

        #endregion

        #region Private Methods

        private void SetNextDestinationPoint()
        {
            currentDestPointIndex = (currentDestPointIndex + 1) % destinationPoints.Count;
            waitingTime = destinationPoints[currentDestPointIndex].PointDuration;
        }

        private bool IsCloseToPoint()
        {
            Vector3 pointDirection = CalculateDistance();

            //change state by comparing square root of substracted position to square of check radius
            return pointDirection.sqrMagnitude <= closeDistance * closeDistance;
        }

        private Vector3 CalculateDistance()
        {
            Vector3 pointPosition = destinationPoints[currentDestPointIndex].PointPosition;

            return pointPosition - transform.position;
        }

        private Vector3 CalculateDirection()
        {
            Vector3 distToPoint = CalculateDistance();

            return distToPoint.normalized;
        }

        private Quaternion CalculateAngle()
        {
            Vector3 direction = CalculateDistance();

            return Quaternion.LookRotation(direction, Vector3.up);
        }

        private void PerformStateAction()
        {
            switch (patrollingState)
            {
                case PatrollingState.Moving :
                    Patrol();

                    break;

                case PatrollingState.Standing :
                    Stay();

                    break;

                case PatrollingState.Searching :
                    Search();

                    break;
            }
        }

        private void Patrol()
        {
            Rotate(CalculateAngle());
            Move(CalculateDirection());

            if (IsCloseToPoint())
            {
                patrollingState = PatrollingState.Standing;
            }
        }

        private void Stay()
        {
            walkPointSet = false;
            Move(new Vector3(0, 0, 0));

            if (HasWaitingTime())
            {
                waitingTime -= Time.deltaTime;
            }
            else
            {
                patrollingState = PatrollingState.Searching;
            }
        }

        private void Search()
        {
            if (!walkPointSet)
            {
                SetNextDestinationPoint();
                walkPointSet = true;
            }

            patrollingState = PatrollingState.Moving;
        }

        private void GetReferences()
        {
            npcMovementController = GetComponent<NpcMovementController>();
        }

        private bool HasWaitingTime() => destinationPoints[currentDestPointIndex].PointDuration >= 0 && waitingTime > 0;

        private void InitializeValues()
        {
            patrollingState = PatrollingState.Moving;
            currentDestPointIndex = 0;
            walkPointSet = true;
        }

        #endregion
    }
}