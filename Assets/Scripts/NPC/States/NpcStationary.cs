using System.Collections;
using NPC.Data;
using NPC.Interfaces;
using UnityEngine;

namespace NPC.States
{
    [RequireComponent(typeof(NpcMovementController))]
    public class NpcStationary : MonoBehaviour, IRotatable
    {
        #region Serialized Fields

        [SerializeField]
        private Point[] lookPoints;

        #endregion

        #region Private Fields

        private NpcMovementController npcMovementController;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            GetReferences();
            StartCycling();
        }

        #endregion

        #region Public Methods

        public void Rotate(Quaternion quaternion)
        {
            npcMovementController.Rotate(quaternion);
        }

        #endregion

        #region Private Methods

        private void StartCycling()
        {
            if (lookPoints.Length > 0)
            {
                StartCoroutine(CycleLookPoints());
            }
        }

        private Vector3 CalculateDirectionToLookPoint(Point point)
        {
            return (point.PointPosition - transform.position).normalized;
        }

        private Quaternion CalculateAngleToLookPoint(Point point)
        {
            Vector3 direction = CalculateDirectionToLookPoint(point);

            return Quaternion.LookRotation(direction, Vector3.up);
        }

        private IEnumerator CycleLookPoints()
        {
            int currentIndex = 0;

            while (true)
            {
                Point currentPoint = lookPoints[currentIndex];
                Rotate(CalculateAngleToLookPoint(currentPoint));

                yield return new WaitForSeconds(currentPoint.PointDuration);
                IterateIndex();
            }

            void IterateIndex()
            {
                currentIndex = (currentIndex + 1) % lookPoints.Length;
            }
        }

        private void GetReferences()
        {
            npcMovementController = GetComponent<NpcMovementController>();
        }

        #endregion
    }
}