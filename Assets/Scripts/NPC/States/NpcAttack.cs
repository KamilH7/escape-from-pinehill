using GameLoop.Data;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;
using Utils.DataVariables;

namespace NPC.States
{
    [RequireComponent(typeof(NpcMovementController))]
    public class NpcAttack : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Vector3Variable playerPosition;
        [SerializeField]
        private float detectionRadius;

        #endregion

        #region Private Fields

        private Collider collider;

        private Animator animator;

        private bool isAlive = true;

        private NpcMovementController npcMovementController;

        #endregion

        #region Constants

        private const string deathTriggerName = "Die";

        #endregion

        #region Public Properties

        public bool IsAlive => isAlive;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReference();
        }

        private void Update()
        {
            if (isAlive && IsPlayerInRange())
            {
                Rotate(GetRotationTowardsPlayer());
                Move(GetDirectionToPlayer());
            }
            else
            {
                Move(Vector3.zero);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.IsPlayer())
            {
                PlayerKilled();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer())
            {
                PlayerKilled();
            }

            if (other.IsBullet())
            {
                Destroy(other.gameObject);
                GetComponent<CapsuleCollider>().enabled = false;
                EnemyKilled();
            }
        }

        #endregion

        #region Private Methods

        private void PlayerKilled()
        {
            Move(Vector3.zero);
            GameEvents.PlayerDied();
        }

        [Button]
        private void EnemyKilled()
        {
            isAlive = false;
            collider.enabled = false;
            animator.SetTrigger(deathTriggerName);
        }

        private Quaternion GetRotationTowardsPlayer()
        {
            return Quaternion.LookRotation(GetDirectionToPlayer(), Vector3.up);
        }

        private Vector3 GetDirectionToPlayer()
        {
            return (playerPosition.Value - transform.position).normalized;
        }

        private bool IsPlayerInRange()
        {
            return Vector3.Distance(playerPosition.Value, transform.position) <= detectionRadius;
        }

        private void GetReference()
        {
            npcMovementController = GetComponent<NpcMovementController>();
            animator = GetComponentInChildren<Animator>();
            collider = GetComponentInChildren<Collider>();
        }

        private void Move(Vector3 vector)
        {
            npcMovementController.Move(vector);
        }

        private void Rotate(Quaternion quaternion)
        {
            npcMovementController.Rotate(quaternion);
        }

        #endregion
    }
}