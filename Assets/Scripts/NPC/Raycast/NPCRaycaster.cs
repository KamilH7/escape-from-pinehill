using UnityEngine;

namespace NPC.Raycast
{
    public class NPCRaycaster : MonoBehaviour
    {
        #region Serialized Fields

        [Header("Settings"), Range(0, 180), SerializeField]
        private int patrollingAngle = 70;
        [Range(2, 20), SerializeField]
        private float raycastDistance = 10;
        [Range(10, 1000), SerializeField]
        private int raycastDensity = 100;
        [SerializeField]
        private LayerMask collidingLayer;

        #endregion

        #region Private Fields

        private NPCRaycastResult[] raycasts;
        private Vector3 currentForward;

        #endregion

        #region Public Properties

        public NPCRaycastResult[] Raycasts => raycasts;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            SetupRaycastDirections();
            Raycast();
        }

        private void Update()
        {
            if (currentForward != transform.forward)
            {
                SetupRaycastDirections();
            }

            Raycast();
        }

        #endregion

        #region Private Methods

        private void Raycast()
        {
            for (int i = 0; i < raycasts.Length; i++)
            {
                MakeRaycast(raycasts[i].Direction, out RaycastHit hitInfo);
                SaveResult(hitInfo, raycasts[i]);
            }
        }

        private void SaveResult(RaycastHit hitInfo, NPCRaycastResult raycast)
        {
            Vector3 hitpoint;

            if (hitInfo.transform != null)
            {
                hitpoint = hitInfo.point;
            }
            else
            {
                hitpoint = transform.position + raycast.Direction * raycastDistance;
            }

            raycast.HitPoint = hitpoint;
            raycast.HitObject = hitInfo.transform;
        }

        private bool MakeRaycast(Vector3 raycastDirection, out RaycastHit hitInfo) => Physics.Raycast(transform.position, raycastDirection, out hitInfo, raycastDistance, collidingLayer);

        private void SetupRaycastDirections()
        {
            raycasts = new NPCRaycastResult[raycastDensity];
            currentForward = transform.forward;

            float angleIncrementRate = patrollingAngle / (float) raycastDensity;

            for (int i = 0; i < raycasts.Length; i++)
            {
                raycasts[i] = new NPCRaycastResult(GetDirection(angleIncrementRate * i - patrollingAngle / 2));
            }
        }

        private Vector3 GetDirection(float angle)
        {
            Vector3 direction = Quaternion.Euler(0, angle, 0) * transform.forward;

            return direction.normalized;
        }

        #endregion
    }
}