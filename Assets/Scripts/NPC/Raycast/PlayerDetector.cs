﻿using GameLoop.Data;
using UnityEngine;
using Utils;

namespace NPC.Raycast
{
    [RequireComponent(typeof(NPCRaycaster))]
    public class PlayerDetector : MonoBehaviour
    {
        #region Private Fields

        private NPCRaycaster raycaster;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
        }

        private void Update()
        {
            HandlePlayerDetection();
        }

        #endregion

        #region Private Methods

        private void HandlePlayerDetection()
        {
            if (IsPlayerVisible())
            {
                GameEvents.PlayerDied();
            }
        }

        private bool IsPlayerVisible()
        {
            foreach (NPCRaycastResult raycast in raycaster.Raycasts)
            {
                if (IsPlayer(raycast.HitObject))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsPlayer(Transform o) => o && o.IsPlayer();

        private void GetReferences()
        {
            raycaster = GetComponent<NPCRaycaster>();
        }

        #endregion
    }
}