using UnityEngine;

namespace NPC.Raycast
{
    public class NPCRaycastResult
    {
        #region Private Fields

        private Vector3 direction;

        #endregion

        #region Public Properties

        public Vector3 Direction => direction;

        public Vector3 HitPoint { get; set; }
        public Transform HitObject { get; set; }

        #endregion

        #region Constructors

        public NPCRaycastResult(Vector3 direction)
        {
            this.direction = direction;
        }

        #endregion
    }
}