using System.Collections.Generic;
using UnityEngine;

namespace NPC.Raycast
{
    [RequireComponent(typeof(NPCRaycaster))]
    public class RaycastIndicator : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private GameObject meshContainerPrefab;

        #endregion

        #region Private Fields

        private NPCRaycaster raycaster;
        private Mesh viewMesh;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            GetReferences();
        }

        private void Start()
        {
            GenerateContainer();
        }

        private void Update()
        {
            GenerateMesh();
        }

        #endregion

        #region Private Methods

        private void GenerateContainer()
        {
            viewMesh = Instantiate(meshContainerPrefab, transform).GetComponent<MeshFilter>().mesh;
        }

        private List<Vector3> GenerateViewPoints()
        {
            List<Vector3> positions = new List<Vector3>();

            foreach (NPCRaycastResult raycast in raycaster.Raycasts)
            {
                positions.Add(raycast.HitPoint);
            }

            return positions;
        }

        private void GenerateMesh()
        {
            List<Vector3> viewPoints = GenerateViewPoints();

            int vertexCount = viewPoints.Count + 1;
            Vector3[] vertices = new Vector3[vertexCount];
            int[] triangles = new int[(vertexCount - 2) * 3];

            vertices[0] = Vector3.zero;

            for (int i = 0; i < vertexCount - 1; i++)
            {
                vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

                if (i < vertexCount - 2)
                {
                    triangles[i * 3] = 0;
                    triangles[i * 3 + 1] = i + 1;
                    triangles[i * 3 + 2] = i + 2;
                }
            }

            viewMesh.Clear();
            viewMesh.vertices = vertices;
            viewMesh.triangles = triangles;
            viewMesh.RecalculateNormals();
        }

        private void GetReferences()
        {
            raycaster = GetComponent<NPCRaycaster>();
        }

        #endregion
    }
}