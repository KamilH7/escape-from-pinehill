using UnityEngine;

namespace Animation
{
    public class AnimationTrigger : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private AnimationParameter[] animationsToTrigger;

        #endregion

        #region Public Methods

        public void TriggerAnimations()
        {
            foreach (AnimationParameter anim in animationsToTrigger)
            {
                anim.Trigger();
            }
        }

        #endregion
    }
}