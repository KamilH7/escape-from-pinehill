using System;
using UnityEngine;

namespace Animation
{
    [Serializable]
    public class AnimationParameter
    {
        #region Serialized Fields

        [SerializeField]
        private AudioPlayer audioPlayer;

        [SerializeField]
        private Animator animator;
        [SerializeField]
        private string triggerName;

        #endregion

        #region Public Methods

        public void Trigger()
        {
            animator?.SetTrigger(triggerName);
            audioPlayer?.Play();
            //lol
        }

        #endregion
    }
}