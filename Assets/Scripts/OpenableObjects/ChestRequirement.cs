using System.Collections.Generic;
using Animation;
using Inventory.Data;
using UnityEngine;
using Utils;

namespace OpenableObjects
{
    public class ChestRequirement : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private List<Item> requiredItems;
        [SerializeField]
        private PlayerInventory inventory;

        #endregion

        #region Private Fields

        private AnimationTrigger animationTrigger;
        private bool isChestOpened;

        #endregion

        #region Unity Callbacks

        private void OnTriggerEnter(Collider other)
        {
            if (other.IsPlayer())
            {
                if (!HasAllRequiredItems())
                {
                    RequirementsNotMetNotification();
                }
                else if (!isChestOpened)
                {
                    isChestOpened = true;
                    AddItemToPlayerInventory();
                    inventory.RemoveItem(requiredItems[0]);
                    RequirementsMetNotification();
                }
            }
        }

        private void Awake()
        {
            GetReferences();
        }

        private void Start()
        {
            InitializeValues();
        }

        #endregion

        #region Private Methods

        private bool HasAllRequiredItems()
        {
            bool hasAllRequiredItems = true;

            foreach (Item item in requiredItems)
            {
                if (!inventory.HasItem(item))
                {
                    hasAllRequiredItems = false;
                }
            }

            return hasAllRequiredItems;
        }

        private void RequirementsNotMetNotification()
        {
            Debug.Log("Player doesn't have required items");
        }

        private void RequirementsMetNotification()
        {
            animationTrigger.TriggerAnimations();
        }

        private void AddItemToPlayerInventory()
        {
            inventory.AddIdCards(1);
        }

        private void GetReferences()
        {
            animationTrigger = GetComponent<AnimationTrigger>();
        }

        private void InitializeValues()
        {
            isChestOpened = false;
        }

        #endregion
    }
}